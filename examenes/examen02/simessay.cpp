#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <list>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <sys/sysinfo.h>

#include "simessay.h"

#define COMPARISONS 1
#define RESULTS 0

using namespace std;

//non-member function
bool comparisonFunction(const result_t& res1, const result_t& res2);

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	Simessay sime(argc, argv); //every process owns a Simessay object
	sime.exec();
	MPI_Finalize();
	return 0;
}

Simessay::Simessay(int argc, char* argv[])
: globalCombinations(0), localCombinations(0), maxLineLen(0), rank(-1), commSZ(-1), linesCount(0), workDistribution(nullptr), lines(nullptr), comps(nullptr), localComps(nullptr), results(nullptr), localResults(nullptr)
{
	MPI_Comm_rank(MPI_COMM_WORLD, &this->rank);
	MPI_Comm_size(MPI_COMM_WORLD, &this->commSZ);
	
	if(argc < 3){
		if(this->rank == 0)
			cout << "Usage: " << argv[0] << " lines_count max_line_length" << endl;
		exit(1);
	}
	
	this->linesCount = atoi(argv[1]);
	this->maxLineLen = atoi(argv[2]);
	this->init();
}

Simessay::~Simessay()
{
	if(this->rank == 0){
		//only process 0 allocates memory for those arrays of global things
		delete [] this->lines;
		delete [] this->comps;
		delete [] this->results;
	}
	delete [] this->localComps;
	delete [] this->localResults;
	delete [] this->workDistribution;
}

void Simessay::init()
{
	this->workDistribution = new int[this->commSZ];
	if(this->rank == 0){
		this->lines = new line_t[this->linesCount];
		this->globalCombinations = (this->linesCount * (this->linesCount - 1)) / 2;
		this->comps = new comparison_t[this->globalCombinations];
		this->results = new result_t[this->globalCombinations];
		this->distributeWork();
	}
}

void Simessay::distributeWork()
{
	int start = 0;
	int end = 0;
	int remainder = 0;
	int quotient = 0;
	quotient = this->globalCombinations / this->commSZ;
	remainder = this->globalCombinations % this->commSZ;
	
	for(int index = 0; index < this->commSZ; ++index){
		start = index * quotient + min(index, remainder);
		end = (index + 1) * quotient + min((index + 1), remainder);
		this->workDistribution[index] = end - start;
	}
}

void Simessay::computeDisplacements(int* displacements, const int isComparisonsDisplacement)
{
	//array of memory offsets for each position of comparisons array
	
	for(int index = 0; index < this->commSZ; ++index){
		if(index == 0){
			displacements[index] = 0;
		}else{
			displacements[index] = this->workDistribution[index - 1] * (isComparisonsDisplacement? sizeof(comparison_t) : sizeof(result_t)) + displacements[index - 1];
		}
	}
}

void Simessay::calculateBytes(int* outputBytes, const int isComparison)
{
	for(int index = 0; index < this->commSZ; ++index){
		outputBytes[index] = this->workDistribution[index] * (isComparison? sizeof(comparison_t) : sizeof(result_t));
	}
}

void Simessay::getInput() //Only process 0 reads the lines, then lists the comparisons and then scatters them to all other procs
{						  
	if(this->rank == 0){
		cout << "Enter the lines to be scanned: " << endl;
		for(int currentLine = 0; currentLine < this->linesCount; ++currentLine){
			cin.getline(this->lines[currentLine].line, this->maxLineLen + 1); 	//+1 for the lf character
			this->lines[currentLine].lineId = currentLine;
		}
	}
}

void Simessay::listCombinations()
{
	if(this->rank == 0){
		list<line_t> linesList;
		for(int index = 0; index < this->linesCount; ++index)
			linesList.push_back(this->lines[index]); //fills the list with the lines
			
		int currLine = 0;

		//lists the comparisons in the global comparisons array by iterating the list with two iterators
		for(auto firstLine = linesList.begin(); firstLine != linesList.end(); ++firstLine){
			for(auto secondLine = firstLine; secondLine != linesList.end(); ++secondLine){
				if(firstLine == secondLine)
					continue;
				
				strcpy(this->comps[currLine].string1, (*firstLine).line);
				strcpy(this->comps[currLine].string2, (*secondLine).line);
				this->comps[currLine].firstLineId = (*firstLine).lineId;
				this->comps[currLine].secondLineId = (*secondLine).lineId;
				++currLine;
			}
		}
	}
}

void Simessay::compare(const comparison_t& comp, result_t& res)
{
	res.distance = levenshtein(comp.string1, comp.string2);	//obtains the Leveshtein's distance between two strings
	res.firstLineId = comp.firstLineId + 1; // + 1: human-readable
	res.secondLineId = comp.secondLineId + 1;
}

void Simessay::exec()
{
	//all the processes call these functions, but only process 0 does some work there
	this->getInput();
	this->listCombinations();
	
	//distributes the number of combinations assigned to each process
	MPI_Bcast(this->workDistribution, this->commSZ, MPI_INT, 0, MPI_COMM_WORLD);
	
	//number of combinations assigned to each process
	this->localCombinations = this->workDistribution[this->rank];
	
	//arrays for each process' comparisons and results
	this->localComps = new comparison_t[this->localCombinations];
	this->localResults = new result_t[this->localCombinations];
	
	//arrays needed by MPI_Scatterv and MPI_Gatherv implementations
	int compsDisplacements[this->commSZ];
	int resultsDisplacements[this->commSZ];
	int compsInBytes[this->commSZ];
	int resultsInBytes[this->commSZ];

	//clear the arrays
	memset(compsDisplacements, 0, this->commSZ);
	memset(resultsDisplacements, 0, this->commSZ);
	memset(compsInBytes, 0, this->commSZ);
	memset(resultsInBytes, 0, this->commSZ);
	
	//calculates the displacements in memory of each chunk of data that each process needs
	this->computeDisplacements(compsDisplacements, COMPARISONS);
	this->computeDisplacements(resultsDisplacements, RESULTS);
	
	//fill the arrays with the size in bytes of the structs they need to send or to receive	
	this->calculateBytes(compsInBytes, COMPARISONS);
	this->calculateBytes(resultsInBytes, RESULTS);
	
	//process 0 distributes the comparisons among the other process, himself included
	MPI_Scatterv(this->comps, compsInBytes, compsDisplacements, MPI_BYTE, this->localComps, compsInBytes[this->rank], MPI_BYTE, 0, MPI_COMM_WORLD);
	
	int thrCount = get_nprocs_conf();
	
	//shared-memory concurrency
	#pragma omp parallel for num_threads(thrCount) default(none) shared(localComps) shared(localResults)
	for(int index = 0; index < this->localCombinations; ++index){
		if(this->localComps[index].string1)
			this->compare(this->localComps[index], this->localResults[index]);
	}

	//all other processes send their results to process 0
	MPI_Gatherv(this->localResults, resultsInBytes[this->rank], MPI_BYTE, this->results, resultsInBytes, resultsDisplacements, MPI_BYTE, 0, MPI_COMM_WORLD);
	
	//process 0 sorts the results by distance and prints them
	if(this->rank == 0){
		this->sort();
		this->printResults();
	}
}

void Simessay::printResults() const
{
	for(int index = 0; index < this->globalCombinations; ++index)
		cout << this->results[index].firstLineId << " " << this->results[index].secondLineId << " " << this->results[index].distance << endl;
}

long Simessay::levenshtein(const char* text1, const char* text2)
{
	unsigned long long rows = 0;
	unsigned long long columns = 0;
	int** matrix;
	int cost = 0;
	int distance = 0;

    rows = (unsigned long long)strlen(text1);
    columns = (unsigned long long)strlen(text2);

    if(!rows) 
		return columns;
    if(!columns)
		return rows;
	//adjust size of matrix to allocate an extra row and an extra column
	++rows;  
	++columns;
	//allocates the matrix in heap
    matrix = (int**) calloc(rows, sizeof(int*));
	matrix[0] = (int*) calloc(columns, sizeof(int));
	matrix[0][0] = 0; //initializes the uppermost left cell to 0
	
    if (!matrix){
		fprintf(stderr, "An error ocurred, not enough space for Levenshtein's matrix!\n");
		return -1;
	}
	//initializes the matrix as levenshtein's algorithm requires
	for(int i = 0; i < (int)columns; ++i)
		matrix[0][i] = i;

	// Fills the matrix with values that depends on three other cells
    for(size_t i = 1; i < rows; i++){
		matrix[i] = (int*) calloc(columns, sizeof(int)); //creates the rows and columns it needs for calculation in execution time
		for(size_t j = 1; j < columns; j++){ 	
			matrix[i][0] = i;
			cost = (text1[i-1] == text2[j-1]? 0 : 1);
			matrix[i][j] = min(min(matrix[i-1][j]+1, matrix[i][j-1]+1), matrix[i-1][j-1]+cost);
		} 
		free(matrix[i-1]);  //then frees the previous row that is not needed anymore, to save space
	}
	// Returns the matrix final corner which contains the final distance

	distance = matrix[rows-1][columns-1];
	free(matrix[rows-1]);
    free(matrix);
    return distance;
}

void Simessay::sort()
{
	list<result_t> sortingList;
	for(int index = 0; index < this->globalCombinations; ++index){
		sortingList.push_back(this->results[index]);
	}

	//comparisonFunction compares two result_t structs based on their distance field
	sortingList.sort(comparisonFunction);
	
	int index = 0;
	
	//the sorted list is copied back to the results array
	for(auto iterator = sortingList.begin(); iterator != sortingList.end(); ++iterator){
		this->results[index++] = *iterator;
	}
}

bool comparisonFunction(const result_t& res1, const result_t& res2)
{
	return (res1.distance < res2.distance ? true : false);
}
