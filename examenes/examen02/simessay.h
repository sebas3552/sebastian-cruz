#ifndef SIMESSAY_H
#define SIMESSAY_H

#define STRING_MAX 1000

/**
 * An opaque record that represents a comparison between two string lines.
 */
typedef struct
{
	///Strings to be compared.
	char string1[STRING_MAX];
	char string2[STRING_MAX];
	
	///Number of each line, in the order in which they were read.
	int firstLineId;
	int secondLineId;
}comparison_t;

/**
 * An opaque record that represents a string line.
 */
typedef struct
{
	///Line read from standard input.
	char line[STRING_MAX];
	///Sequential number of the read line.
	int lineId;
}line_t;

/**
 * An opaque record that contains the final data to be printed to the user.
 */ 
typedef struct
{
	///Number of each line, in the order in which they were read.
	int firstLineId;
	int secondLineId;
	///Distance calculated between those lines.
	long distance;
}result_t;

/**
 * @class Simessay
 * Methods of this class are meant to be executed concurrently in a network, from different objects at the same time.
 * The purpose of this class is to list the minimum edit distances between strings entered by the user in the standard input.
 */
class Simessay
{
	private:
		///Method used to initialize a Simessay object, depending whether this object is allocated on process 0 or not.
		void init();
		
		///Method that reads the user input.
		void getInput();
		
		///Method that lists the combinations between the lines.
		void listCombinations();
		
		///Simple method that prints out the results.
		void printResults() const;
		
		///Method that sorts the results.
		void sort();
		
		///Method executed on the root process that distributes the number of comparisons each process has to work out.
		void distributeWork();
		
		/**
		 * Method that compares two strings using Levenshtein's minimum edit distance and sets the results in a result_t struct.
		 * @param comp Comparison to be worked.
		 * @param res Struct where the results are going to be placed.
		 */
		void compare(const comparison_t& comp, result_t& res);
		
		/**
		 * Method that calculates the memory offsets for each position in a comparison struct or a results struct array.
		 * @param displacements Array to fill with the calculations.
		 * @param isComparisonDisplacement Flag to choose between offsets for comparison_t struct (1) and result_t struct (0).
		 */
		void computeDisplacements(int* displacements, const int isComparisonsDisplacement);
		
		/**
		 * Method that calculates the size in bytes of the data that should be sent or received by the processes.
		 * @param outputBytes Array to fill with the calculations.
		 * @param isComparison Flag to choose between calculate the size of comparison_t structs (1) and the size of result_t structs (0).
		 */
		void calculateBytes(int* outputBytes, const int isComparison);
		
		/**
		 * Subroutine that calculates the Levenshtein's minimum edit distance between two strings.
		 * @param text1 First string to be compared.
		 * @param text2 Second string to be compared.
		 * @return the distance between those strings.
		 */
		long levenshtein(const char* text1, const char* text2);

		///Number of total combinations between the lines read from the input. Significant only at root process.
		int globalCombinations;
		
		///Number of combinations assigned to the non-root process that holds this Simessay object.
		int localCombinations;
		
		///Max length of the strings entered by the user. Entered by command-line arguments.
		int maxLineLen;
		
		///Rank of the process that holds this Simessay object.
		int rank;
		
		///Size of the communication world.
		int commSZ;
		
		///Number of lines entered by the user. Also entered by command-line arguments.
		int linesCount;
		
		///Integer array that holds in its ith entry the number of comparisons assigned to the ith process.
		int* workDistribution;
		
		///Array of line_t structs holding the lines entered by the user. Significant only at root process.
		line_t* lines;
		
		///Array of the total comparisons listed between all the lines. Significant only at root process.
		comparison_t* comps;
		
		///Array of the local comparisons assigned to the process that holds this Simessay object.
		comparison_t* localComps;
		
		///Array of the total results of the program. Significant only at root process.
		result_t* results;
		
		///Array of the local results calculated by the process that holds this Simessay object.
		result_t* localResults;
		
	public:
	
		/**
		 * Constructor.
		 * @param argc Argument count provided by the main function.
		 * @param argv Argument vector provided by the main function.
		 */ 
		Simessay(int argc, char* argv[]);
		
		///Destructor.
		~Simessay();
		
		///Main method that executes concurrently the task described in the class header.
		void exec();
};

#endif
