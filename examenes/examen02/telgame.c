#include <assert.h>
#include <ctype.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "telgame.h"

#define RANDOMIZE (srand(time(NULL) + clock()))
#define MESSAGE_MAX 10000000

int main(int argc, char* argv[])
{
	int comm_sz = -1;
	int comm_rank = -1;
	
	MPI_Init(&argc, &argv);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	
	if(argc < 3){
		if(comm_rank == 0)
			printf("Usage: %s prob_chg_vowel prob_chg_consonant [rounds]\n", argv[0]);
		exit(1);
	}
	
	double prob_change_vowel = 0.0;
	double prob_change_consonant = 0.0;
	
	//reads the probabilities given by the user
	prob_change_vowel = atof(argv[1]);
	prob_change_consonant = atof(argv[2]);
	
	//default rounds = 1
	int rounds = 1;
	
	//reads the number of rounds, if the user entered any
	if(argc >= 4)
		rounds = atoi(argv[3]);

	//allocates memory for a message of maximum size 10 MB
	char* message = (char*) calloc(MESSAGE_MAX, sizeof(char));
	
	//Only root process reads the message and then passes it to the next process
	if(comm_rank == 0)
		read_message(message);

	int still_playing = 1;
	
	//Root process starts the game
	if(comm_rank == 0){
		for(int iteration = 0; iteration < rounds; ++iteration){
			
			//distort the message
			distort_message(message, prob_change_vowel, prob_change_consonant);

			//send the message to the next process
			MPI_Send(message, strlen(message) + 1, MPI_CHAR, (comm_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
			
			if(iteration == rounds - 1)
				still_playing = 0;	//game over
				
			//send the still_playing flag to the next process
			MPI_Send(&still_playing, 1, MPI_INT, (comm_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
			
			//wait to receive the message after it circulated through the other processes
			MPI_Recv(message, strlen(message) + 1, MPI_CHAR, comm_sz - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		puts(message); 	//print the final message
	}else{
		do{
			//wait to receive the message from the previous process
			MPI_Recv(message, MESSAGE_MAX, MPI_CHAR, comm_rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			
			//wait to receive the still_playing flag from the previous process
			MPI_Recv(&still_playing, 1, MPI_INT, comm_rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			
			//distort the message
			distort_message(message, prob_change_vowel, prob_change_consonant);
			
			//send the message to the next process
			MPI_Send(message, strlen(message) + 1, MPI_CHAR, (comm_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
			
			//if i'm the last process, don't send the flag
			if(comm_rank != comm_sz-1)
				MPI_Send(&still_playing, 1, MPI_INT, (comm_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
				
		}while(still_playing);
	}
	
	free(message);
	
	MPI_Finalize();
		
	return 0;
	
}

bool is_vowel(char character)
{
	char vowels[] = "aeiouAEIOU";
	if(isalpha(character) && strchr(vowels, character))
		return true;
	else
		return false;
}

bool is_consonant(char character)
{
	if(isalpha(character) && !is_vowel(character))
		return true;
	else
		return false;
}

bool modify_char(double probability)
{
	//rescale the probability to choose a random number between 0 and 100
	probability *= 100.0;
	
	RANDOMIZE;
	
	if((rand() % 100) <= probability)
		return true;
	else
		return false;
}

void distort_message(char* message, double prob_chg_vowel, double prob_chg_cons)
{
	int chosen_char_pos = 0;
	
	RANDOMIZE;
	
	assert(strlen(message) > 0);
	
	if(modify_char(prob_chg_vowel)){	//if the probability gave the chance to modify a vowel
		do{
			chosen_char_pos = rand() % strlen(message);	//pick a vowel randomly from the string
		}while(!is_vowel(message[chosen_char_pos]));
		
		char vowels[] = "aeiou";
		
		//Mantains the string format by choosing a uppercase or a lowercase letter depending on the original letter.
		if(isupper(message[chosen_char_pos]))
			message[chosen_char_pos] = toupper(vowels[rand() % strlen(vowels)]);	//change the vowel
		else
			message[chosen_char_pos] = vowels[rand() % strlen(vowels)];
	}else{
		if(modify_char(prob_chg_cons)){		//if the probability gave the chance to modify a consonant
			do{
				chosen_char_pos = rand() % strlen(message);	//pick a consonant randomly from the string
			}while(!is_consonant(message[chosen_char_pos]));
			
			char consonants[] = "bcdfghjklmnpqrstvwxyz";
			
			if(isupper(message[chosen_char_pos]))	//change the consonant
				message[chosen_char_pos] = toupper(consonants[rand() % strlen(consonants)]);
			else
				message[chosen_char_pos] = consonants[rand() % strlen(consonants)];
		}
	}
}

void read_message(char* buffer)
{
	puts("Enter the message and press ctrl-d to finish:");
	int pos = 0;
	char ch = '\0';
	
	do{
		ch = fgetc(stdin);
		if(ch != EOF && pos < MESSAGE_MAX)
			buffer[pos++] = ch;
	}while(ch != EOF);
}
