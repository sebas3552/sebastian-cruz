#ifndef TELGAME_H
#define TELGAME_H

#include <stdbool.h>

///Main subroutine that executes the game concurrently.
int main(int argc, char* argv[]);

///Function that decides randomly whether to modify a char from the string or not, based on the given probability.
bool modify_char(double probability);

///Function that returns true if the entered character is a consonant, and false otherwise.
bool is_consonant(char character);

///Function that returns true if the entered character is a vowel, and false otherwise.
bool is_vowel(char character);

/**
 * Function that may change a randomly chosen character of the message, based on the given probabilities.
 * @param message Message to be manipulated.
 * @param prob_chg_vowel The probability of changing a vowel of the message, given by the user.
 * @param prob_chg_cons The probability of changing a consonant of the message, given by the user.
 */
void distort_message(char* message, double prob_chg_vowel, double prob_chg_cons);

///Function that reads in the message from standard input. Must be called only by root process' main thread.
void read_message(char* buffer);

#endif
