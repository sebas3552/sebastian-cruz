#include <assert.h>
#include <math.h>
#include <omp.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include "levenshtein.h"
//extended Ascii charset
#define SIGMA_ASCII 256
//little subset of Unicode charset
#define SIGMA_UNICODE 16384
#define min(a, b) (a < b ? a : b)

size_t levenshtein_distance_ascii(const char* string_1, const char* string_2, size_t requested_workers)
{
	size_t worker_count = requested_workers;
	const unsigned char* p = (const unsigned char*)string_1;
	const unsigned char* t = (const unsigned char*)string_2;
	size_t m = strlen(string_1);
	size_t n = strlen(string_2);
	//more workers than characters of the smallest string is not allowed, doesn't make sense
	if(m < worker_count || n < worker_count){
		worker_count = min(m, n);
		if(worker_count == 0)
			++worker_count;
	}
	unsigned long long** x;
	unsigned long long** d;
	unsigned char alphabet[SIGMA_ASCII];
	assert(&alphabet[0]);
	levenshtein_init_ascii_alphabet(&alphabet[0]);

	x = (unsigned long long**) calloc(SIGMA_ASCII, sizeof(unsigned long long*));
	assert(x);
	for(size_t j = 0; j < SIGMA_ASCII; ++j){
		x[j] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long*));
		assert(x[j]);
	}
	
	//parallel initialization of matrix x by rows
	#pragma omp parallel for num_threads(worker_count) default(none) \
	shared(t, x, n, alphabet)
	for(size_t row = 0; row < SIGMA_ASCII; ++row){
		for(size_t column = 0; column <= n; ++column){
			if(column == 0){
				x[row][column] = 0;
			}else{
				if(t[column-1] == alphabet[row]){
					x[row][column] = column;
				}else{
					x[row][column] = x[row][column-1];
				}
			}
		}
	}
	//matrix d with just two rows at a time, to save memory
	d = (unsigned long long**) calloc(m+1, sizeof(unsigned long long*));
	d[0] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long));
	
	size_t i = 0;
	//thread pool to mantain the team 'alive' between rows
	#pragma omp parallel num_threads(worker_count) default(none) \
		shared(d, x, p, t, m, n, i)
	{
		size_t calculated_columns = 0;
		while(i <= m){
			//just one thread must allocate and free memory for rows on demand
	#		pragma omp single
			{
				if(i >= 1){
					d[i] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long*));
				}
				
				if(i >= 2){
					free(d[i-2]);
				}
			}
			//counter for each thread to apply the improvement
			calculated_columns = 0;
			//parallel filling of matrix d by columns
	#		pragma omp for 
			for(size_t j = 0; j <= n; ++j){
				if(i == 0){	
					d[i][j] = j;
				}else{
					if(j == 0){
						d[i][j] = i;
					}else{
						if(t[j-1] == p[i-1]){
							d[i][j] = d[i-1][j-1];
						}else{
							//improvement over original parallel algorithm
							if(calculated_columns == 0){
								if(x[p[i-1]][j] == 0){
									d[i][j] = 1 + min(min(d[i-1][j], d[i-1][j-1]), (i + j - 1));
								}else{
									d[i][j] = 1 + min(min(d[i-1][j], d[i-1][j-1]), d[i-1][x[p[i-1]][j]-1] + (j - 1 - x[p[i-1]][j]));
								}
							}else{
								d[i][j] = 1 + min(min(d[i-1][j-1], d[i-1][j]), d[i][j-1]);
							}
						}
					}
				}
				++calculated_columns;
			}
			//just one thread must increment the rows index
	#		pragma omp single
				++i;
		}
	}
	
	for(size_t index = 0; index < SIGMA_ASCII; ++index)
		free(x[index]);
	free(x);
	//minimum edit distance
	size_t distance = d[m][n];
	free(d[m]);
	if(m > 0)
		free(d[m-1]);
	free(d);
	return distance;
}

size_t levenshtein_distance_unicode(const wchar_t* string_1, const wchar_t* string_2, size_t requested_workers)
{
	//same than Ascii distance, changing char for wchar_t and SIGMA_ASCII for SIGMA_UNICODE
	size_t worker_count = requested_workers;
	const wchar_t* p = string_1;
	const wchar_t* t = string_2;
	//wide-char string length
	size_t m = wcslen(string_1);
	size_t n = wcslen(string_2);
	if(m < worker_count || n < worker_count){
		worker_count = min(m, n);
		if(worker_count == 0)
			++worker_count;
	}
	unsigned long long** x;
	unsigned long long** d;
	wchar_t alphabet[SIGMA_UNICODE];
	assert(&alphabet[0]);
	levenshtein_init_unicode_alphabet(&alphabet[0]);

	x = (unsigned long long**) calloc(SIGMA_UNICODE, sizeof(unsigned long long*));
	assert(x);
	for(size_t j = 0; j < SIGMA_UNICODE; ++j){
		x[j] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long*));
		assert(x[j]);
	}
	
	//parallel initialization of matrix x by rows
	#pragma omp parallel for num_threads(worker_count) default(none) \
	shared(t, x, n, alphabet)
	for(size_t row = 0; row < SIGMA_UNICODE; ++row){
		for(size_t column = 0; column <= n; ++column){
			if(column == 0){
				x[row][column] = 0;
			}else{
				if(t[column-1] == alphabet[row]){
					x[row][column] = column;
				}else{
					x[row][column] = x[row][column-1];
				}
			}
		}
	}
	
	d = (unsigned long long**) calloc(m+1, sizeof(unsigned long long*));
	d[0] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long));

	size_t i = 0;
	//thread pool to mantain the team 'alive' between rows
	#pragma omp parallel num_threads(worker_count) default(none) \
		shared(d, x, p, t, m, n, i)
	{
		size_t calculated_columns = 0;
		while(i <= m){
			//just one thread must allocate and free memory for rows on demand
	#		pragma omp single
			{
				if(i >= 1){
					d[i] = (unsigned long long*) calloc(n+1, sizeof(unsigned long long*));
				}
				
				if(i >= 2){
					free(d[i-2]);
				}
			}
			//counter for each thread to apply the improvement
			calculated_columns = 0;
			//Parallel filling of matrix d by columns
	#		pragma omp for 
			for(size_t j = 0; j <= n; ++j){
				if(i == 0){	
					d[i][j] = j;
				}else{
					if(j == 0){
						d[i][j] = i;
					}else{
						if(t[j-1] == p[i-1]){
							d[i][j] = d[i-1][j-1];
						}else{
							//improvement over original parallel algorithm
							if(calculated_columns == 0){
								if(x[p[i-1]][j] == 0){
									d[i][j] = 1 + min(min(d[i-1][j], d[i-1][j-1]), (i + j - 1));
								}else{
									d[i][j] = 1 + min(min(d[i-1][j], d[i-1][j-1]), d[i-1][x[p[i-1]][j]-1] + (j - 1 - x[p[i-1]][j]));
								}
							}else{
								d[i][j] = 1 + min(min(d[i-1][j-1], d[i-1][j]), d[i][j-1]);
							}
						}
					}
				}
				++calculated_columns;
			}
			//just one thread must increment the rows index
	#		pragma omp single
				++i;
		}
	}
	
	for(size_t index = 0; index < SIGMA_UNICODE; ++index)
		free(x[index]);
	free(x);
	size_t distance = d[m][n];
	free(d[m]);
	if(m > 0)
		free(d[m-1]);
	free(d);
	return distance;
}

void levenshtein_init_ascii_alphabet(unsigned char* alphabet)
{
	for(size_t i = 0; i < SIGMA_ASCII; ++i){
		alphabet[i] = (unsigned char) i; //fills the alphabet with standard ASCII charset
	}
}

void levenshtein_init_unicode_alphabet(wchar_t* alphabet)
{
	for(size_t i = 0; i < SIGMA_UNICODE; ++i){
		alphabet[i] = (wchar_t) i; //fills the alphabet with standard UNICODE charset
		assert((wchar_t) i < SIGMA_UNICODE);
	}
}
