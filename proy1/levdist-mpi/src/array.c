#include "array.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

array_t* array_create(size_t size)
{
	array_t* new_array = (array_t*) calloc(1, sizeof(array_t));
	new_array->max_size = size;
	//creates an empty array of char*
	if((new_array->data = (char**) calloc(size, sizeof(char*))))
		return new_array;
	else
		return 0;
}

size_t array_length(array_t* array)
{
	return array->max_size;
}

void array_destroy(array_t* victim)
{
	assert(victim);
	assert(victim->data);
	for(int i = (int) victim->current_element-1; i >= 0; --i){
		assert(victim->data[i]);
		//frees all single elements (char*) in the array
		if(victim->data[i])
			free(victim->data[i]);
	}
	//then, frees the 'main' pointer
	free(victim->data);
	//then, kills the 'victim' struct array_t
	free(victim);
}

int array_push(array_t* array, char* string)
{
	assert(array);
	assert(string);
	//if the capacity has not been exceeded
	if(array->current_element < array_length(array)){
		//add an element to the array
		array->data[array->current_element++] = string;
		return 0;
	}else{
		//else, an error occurred, no more elements can be appended
		return -1;
	}
}

void array_qsort(char** data, int start, int end)
{
	int i, j, center;
	char* pivot;
	center = (start+end)/2;
	pivot = data[center];
	i = start;
	j = end;
	do{
		while(strcmp(data[i], pivot) < 0? 1 : 0)
			++i;
		while(strcmp(data[j], pivot) > 0? 1 : 0)
			--j;
		if(i <= j){
			char* temp;
			temp = data[i];
			data[i] = data[j];
			data[j] = temp;
			++i;
			--j;
		}
	}while(i <= j);
	if(start < j)
		array_qsort(data, start, j);
	if(i < end)
		array_qsort(data, i, end);
}

void array_qsort_by_distance(char** data, int start, int end)
{
	int i, j, center;
	char* pivot;
	center = (start+end)/2;
	pivot = data[center];
	i = start;
	j = end;
	do{
		while(array_get_distance(data[i]) < array_get_distance(pivot))
			++i;
		while(array_get_distance(data[j]) > array_get_distance(pivot)){
			--j;
		}
		if(i <= j){
			if(array_get_distance(data[i]) != array_get_distance(data[j])){
				char* temp;
				temp = data[i];
				data[i] = data[j];
				data[j] = temp;
				++i;
				--j;
			}else{
				++i;
				--j;
			}
		}
	}while(i <= j);
	if(start < j)
		array_qsort_by_distance(data, start, j);
	if(i < end)
		array_qsort_by_distance(data, i, end);
}

array_t* array_sort(array_t* array)
{
	//sorts the whole array
	array_qsort(array->data, 0, (int)array_length(array)-1);
	return array;
}

void array_sort_by_parts(array_t* array)
{
	int start = 0;
	int end = 0;
	for(int i = 1; i < (int)array_length(array); ++i){
		if(array_get_distance(array->data[i]) == array_get_distance(array->data[i-1])){ //if two contiguous strings have the same distance number
			start = i-1; 
			end = i;
			for(int j = end+1;j < (int)array_length(array) && (array_get_distance(array->data[j]) == array_get_distance(array->data[j-1])); ++j){ //look for more contiguous strings with the same distance number
				++end;
			}
		sort_sub_array(array->data, start, end); //sort the subarray alphabetically
		i = end; //continue after the last element of the subarray previously sorted
		}
		
	}
}

void sort_sub_array(char** sub_array, int start, int end)
{
	int elements = (end-start)+1;
	char** copy = calloc(elements, sizeof(char*)); //temporary array to sort
	for(int i = 0; i < elements; ++i){
		copy[i] = sub_array[i+start];
	}
	array_qsort(copy, 0, elements-1); //sort the subarray alphabetically
	for(int i = 0; i < elements; ++i){
		sub_array[i+start] = copy[i]; //copy back the sorted strings into the original subarray
	}
	free(copy);
}

int array_get_distance(char* string)
{
	assert(string);
	char *number = (char*)calloc(1, sizeof(char)); //space for null character
	number[0] = '\0';
	for(int i = 0; string[i] != '\0' && string[i] != '\t'; ++i){
		number = (char*)realloc(number, strlen(number) + 2);
		number[i] = string[i];
		number[i+1] = '\0';
	}
	number = (char*)realloc(number, strlen(number) + 1);
	int distance = atoi(number);
	free(number);
	return distance;
}

