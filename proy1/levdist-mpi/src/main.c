#include "levdist.h"
#include <locale.h>
#include <mpi.h>

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	setlocale(LC_ALL, "");
	// Create a controller "object"
	levdist_t levdist;
	levdist_init(&levdist);
	// Run the command
	int status = levdist_run(&levdist, argc, argv);
	MPI_Finalize();
	return status;
}
