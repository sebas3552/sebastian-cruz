#ifndef LEVDIST_H
#define LEVDIST_H

#define FILENAME_MAX 4096
#define RESULT_MAX 2 * FILENAME_MAX

/** @file levdist.h

This file contains the structures and functions that control the entire
`levdist` command. It can be thought as the global controller object
in object-oriented programming.

It may be created in the main function, for example:

```
int main(int argc, char* argv[])
{
	levdist_t levdist;
	levdist_init(&levdist);
	return levdist_run(&levdist, argc, argv);
}
```
*/

#include "arguments.h"
#include "queue.h"
#include "array.h"



/// Stores the atributes shared for the controller functions in this file.
typedef struct
{
	/// What user asked by the command-line arguments.
	arguments_t arguments;
	/// Queue of files to be compared using Levenshtein distance.
	queue_t* files;
	/// Holds the sorted results of all the comparisons.
	array_t* results;
} levdist_t;


/// Stores the file names of a comparison. 
typedef struct
{
	char file_1[FILENAME_MAX];
	char file_2[FILENAME_MAX];
}comparison_t;


/// Stores the result of a comparison. Note: RESULT_MAX must be 2 * FILENAME_MAX. 
typedef struct
{
	char result[RESULT_MAX];
}result_t;

/**
Initializes a record with default information required by the controller.
@param this Pointer to the @a levdist_t structure to be initialized.
*/
void levdist_init(levdist_t* this);


/**
Start the execution of the command

@param this Pointer to the @a levdist_t structure with the shared attibutes.
@param argc Argument count provided from the `main()` function.
@param argv Argument vector provided from the `main()` function.
@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_run(levdist_t* this, int argc, char* argv[]);


/**
Finds all files in the directories given by arguments, and load them to
the @a this->files queue.
@param this Pointer to the @a levdist_t structure with the shared attibutes.
@param argc Argument count provided from the `main()` function.
@param argv Argument vector provided from the `main()` function.
@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_process_dirs(levdist_t* this, int argc, char* argv[]);


/**
Traverses all the directories provided by user in command-line arguments, and
load all files to the @a this->files queue.

@param this Pointer to the @a levdist_t structure with the shared attibutes.
@param argc Argument count provided from the `main()` function.
@param argv Argument vector provided from the `main()` function.
@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_list_files_in_args(levdist_t* this, int argc, char* argv[]);

/// Function that calculates the number of combinations needed to compare all files against all.
unsigned int levdist_combinations(unsigned int n);

/// Frees dinamically-allocated memory of a levdist struct.
void levdist_finalize(levdist_t* this);

/// Returns the communication world size.
int levdist_world_size();

/// Return the rank of the calling process into the communicator.
int levdist_my_rank();

/**
Fills an array making pairs of files to be compared, from the queue files.
@param queue Pointer to the queue that contains the file list from the input.
@return A pointer to an array of comparison structs of the size of the combinations of the queue files. 
*/
comparison_t* levdist_list_comparisons(queue_t* queue);

/**
Distributes the amount of comparisons fairly among the processes.
@param comparisons Total number of comparisons to be performed.
@param nprocs Number of processors in the communicator.
@return A pointer to an integers array containing the number of comparisons that process i should do. 
*/
int* levdist_distribute_load(int comparisons, int nprocs);

/**
Calculates the memory offset for each position of the comparisons array. Is used only by MPI_Gatherv and
MPI_Scatterv functions, to distribute the comparisons array among the processes.
@param work_distribution Pointer to an integer array which contains the number of comparisons assigned to
process i.
@return A pointer to an integer array which contains the memory offset from the beggining of the comparisons 
array through each position i in the array.
*/
int* levdist_compute_displacements(int* work_distribution);

/**
Initializes a results array by filling with null characters the characters strings for each result struct.
@param results Pointer to a result struct array.
@param result_count Length of the array. 
*/
void levdist_init_results(result_t* results, int result_count);

#endif // LEVDIST_H
