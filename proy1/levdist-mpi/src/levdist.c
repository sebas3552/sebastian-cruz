#include <assert.h>
#include <limits.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dir.h"
#include "levdist.h"
#include "levenshtein.h"

#define min(a, b) (a < b ? a : b)

// Private functions:

/// Shows how to travese the list removing elements
void levdist_print_and_destroy_files(levdist_t* this);

/// Shows how to traverse the list without removing elements
void levdist_print_files(levdist_t* this);


void levdist_init(levdist_t* this)
{
	arguments_init(&this->arguments);
	this->files = NULL;
	this->results = NULL;
}

int levdist_run(levdist_t* this, int argc, char* argv[])
{
	//Just master process is allowed to do argument analysis.
	if(levdist_my_rank() == 0){
		// Analyze the arguments given by the user
		this->arguments = arguments_analyze(argc, argv);

		// If arguments are incorrect, stop
		if ( this->arguments.error )
			return this->arguments.error;

		// If user asked for help or software version, print it and stop
		if ( this->arguments.help_asked )
			return arguments_print_usage();
		if ( this->arguments.version_asked )
			return arguments_print_version();

		// If user did not provided directories, stop
		if ( this->arguments.dir_count <= 0 )
			return fprintf(stderr, "levdist: error: no directories given\n"), 1;
	}
	// Arguments seems fine, process the directories
	return levdist_process_dirs(this, argc, argv);
}

int levdist_process_dirs(levdist_t* this, int argc, char* argv[])
{
	// Start counting the entire time
	MPI_Barrier(MPI_COMM_WORLD);
	double start_whole_time = MPI_Wtime();
	
	comparison_t* comparisons = NULL;
	comparison_t* local_comparisons = NULL;
	result_t* results = NULL;
	result_t* local_results = NULL;
	int* work_distribution = NULL;
	int* displacements = NULL;
	int local_comparisons_count = 0;
	int comparisons_count = 0;
	int unicode_asked = 0;
	
	
	// Process 0 loads all files into a queue
	if(levdist_my_rank() == 0){
		this->files = queue_create();
		levdist_list_files_in_args(this, argc, argv);
		// If queue has just one file, error
		if(queue_count(this->files) < 2){
			fprintf(stderr, "levdist: error: at least two files are required to compare\n");
			levdist_finalize(this);
			return -1;
		}
		queue_sort(this->files); //sort aphabetically first
		int queue_size = queue_count(this->files);
		unsigned long long combinations = 0;
		
		combinations = levdist_combinations(queue_size); //number of combinations of pairs of files
		comparisons_count = combinations;
		this->results = array_create(combinations);	//final results array
		if(!this->results){
			fprintf(stderr, "An error ocurred, no more memory available!\n");
			levdist_finalize(this);
			return -1;
		}
		//lists each pair of comparisons between files
		comparisons = levdist_list_comparisons(this->files);
		//calculates how many comparisons should be assigned to each process
		work_distribution = levdist_distribute_load(combinations, levdist_world_size());
		//calculates displacements for each position of comparisons array, for MPI_Scatterv and MPI_Gatherv
		displacements = levdist_compute_displacements(work_distribution);
		
		//send the displacements to all processes so that they can use them in their calls to these functions
		MPI_Bcast(displacements, levdist_world_size(), MPI_INT, 0, MPI_COMM_WORLD);
		
		//that's the amount of comparisons assigned to process 0
		local_comparisons_count = work_distribution[levdist_my_rank()];
		
		//creates the array to store the comparisons that process 0 has to do
		local_comparisons = (comparison_t*) calloc(local_comparisons_count, sizeof(comparison_t));

		for(int index = 0; index < levdist_world_size(); ++index){
			//obtains the number of bytes that process needs to receive
			work_distribution[index] *= sizeof(comparison_t);
		}
		
		//Master process sends the number of comparisons assigned to each process
		MPI_Bcast(work_distribution, levdist_world_size(), MPI_INT, 0, MPI_COMM_WORLD);
		
		//Scatters comparisons and sends them to slave processes
		MPI_Scatterv(comparisons, work_distribution, displacements, MPI_BYTE, local_comparisons, local_comparisons_count * sizeof(comparison_t), MPI_BYTE, 0, MPI_COMM_WORLD);

		//Verifies if unicode flag was set, and broadcasts the result to other processes
		unicode_asked = this->arguments.unicode;
		MPI_Bcast(&unicode_asked, 1, MPI_INT, 0, MPI_COMM_WORLD);
		
	}else{
		//Slave processes allocate memory for displacements array
		displacements = (int*) calloc(levdist_world_size(), sizeof(int));
		
		//Slave processes receive the displacements vector for MPI_Scatterv and MPI_Gatherv
		MPI_Bcast(displacements, levdist_world_size(), MPI_INT, 0, MPI_COMM_WORLD);
		
		//Slave processes allocate memory for the work distribution array
		work_distribution = (int*) calloc(levdist_world_size(), sizeof(int));
		MPI_Bcast(work_distribution, levdist_world_size(), MPI_INT, 0, MPI_COMM_WORLD);
		
		//Divide by sizeof(comparison_t) because it was multiplied before to obtain the amount of bytes
		local_comparisons_count = work_distribution[levdist_my_rank()] / sizeof(comparison_t);
		
		//Allocates memory for the process' local comparisons array
		local_comparisons = (comparison_t*) calloc(local_comparisons_count, sizeof(comparison_t));

		//Slave processes receive from the master the comparisons they were assigned
		MPI_Scatterv(comparisons, work_distribution, displacements, MPI_BYTE, local_comparisons, local_comparisons_count * sizeof(comparison_t), MPI_BYTE, 0, MPI_COMM_WORLD);
		
		//Slave processes receive the unicode flag from master process
		MPI_Bcast(&unicode_asked, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}
	//All processes must allocate memory for their results, since all must work on the comparisons
	local_results = (result_t*) calloc(local_comparisons_count, sizeof(result_t));
	
	//Clean un all the char strings
	levdist_init_results(local_results, local_comparisons_count);
	
	// Pair of files for ASCII and UNICODE options (ASCII default)
	char* file_1;
	char* file_2;
	wchar_t* unicode_file_1;
	wchar_t* unicode_file_2;
	int distance = 0;
	
	//Start counting the comparisons time
	MPI_Barrier(MPI_COMM_WORLD);
	double start_comparison_time = MPI_Wtime();
		
	for(int index = 0; index < local_comparisons_count; ++index){
		if(!strcmp(local_comparisons[index].file_1, local_comparisons[index].file_2)){ //if the file names are the same, error
			fprintf(stderr, "levdist: error: at least two files are required to compare\n");
			levdist_finalize(this);
			return -1;
		}
		//Switches between comparisons in ASCII or UNICODE mode.
		if(unicode_asked){
			unicode_file_1 = dir_load_file_as_unicode(local_comparisons[index].file_1);
			unicode_file_2 = dir_load_file_as_unicode(local_comparisons[index].file_2);
			if(!unicode_file_1 || !unicode_file_2){ //if files couldn't be loaded, return
				levdist_finalize(this); 
				return -1;	
			}
			distance = (int)levenshtein_distance_unicode(unicode_file_1, unicode_file_2, this->arguments.workers); //distance with unicode characters
			free(unicode_file_1);
			free(unicode_file_2);
		}else{
			file_1 = dir_load_file(local_comparisons[index].file_1);
			file_2 = dir_load_file(local_comparisons[index].file_2);
			if(!file_1 || !file_2){ //if files couldn't be loaded, return
				levdist_finalize(this);
				return -1;	
			}
			distance = (int)levenshtein_distance_ascii(file_1, file_2, this->arguments.workers); //distance with ASCII characters
			free(file_1);
			free(file_2);
		}
		if(distance == -1){
			levdist_finalize(this);
			return -1;
		}
		//Writes the result of each comparison in the respective results struct
		sprintf(local_results[index].result, "%d\t%s\t%s", (int) distance, local_comparisons[index].file_1,local_comparisons[index].file_2);
	}
	
	double total_comparison_time = MPI_Wtime() - start_comparison_time;
	double max_total_comparison_time = 0.0;
	
	//Takes the maximum comparison time of all the process an gives it to the master process
	MPI_Reduce(&total_comparison_time, &max_total_comparison_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	
	if(levdist_my_rank() == 0){
		//Master process allocates memory for the global results array
		results = (result_t*) calloc(comparisons_count, sizeof(result_t));
		
		//Then he gathers the local results from all slave processes into the global results array
		MPI_Gatherv(local_results, local_comparisons_count * sizeof(result_t), MPI_BYTE, results, work_distribution, displacements, MPI_BYTE, 0, MPI_COMM_WORLD);
		
		//Pushes all the results in a sorteable array, for simplicity
		for(int index = 0; index < comparisons_count; ++index)
			array_push(this->results, strdup(results[index].result));
			
		//Sorts the results first by distance and then alphabetically if some distances are repeated
		array_qsort_by_distance(this->results->data, 0, array_length(this->results)-1);
		array_sort_by_parts(this->results);
		
		//Do not print anything
		if(!this->arguments.silent){
			for(int i = 0; i < (int)array_length(this->results); ++i){
				puts(this->results->data[i]);
			}
		}
		
	}else{
		//Slave processes send their local comparisons results to the master
		MPI_Gatherv(local_results, local_comparisons_count * sizeof(result_t), MPI_BYTE, results, work_distribution, displacements, MPI_BYTE, 0, MPI_COMM_WORLD);
	}
	//All processes deallocate memory
	free(local_results);
	free(local_comparisons);
	free(displacements);
	free(work_distribution);
	//Memory allocated just by the master
	if(results)
		free(results);
	if(comparisons)
		free(comparisons);

	double total_whole_time = MPI_Wtime() - start_whole_time;
	double max_total_whole_time = 0.0;
	
	//Takes the maximum time elapsed of all the processes and gives it to master process
	MPI_Reduce(&total_whole_time, &max_total_whole_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	
	//Report elapsed time, if quiet or silent arguments are not set
	if(levdist_my_rank() == 0 && !this->arguments.quiet && !this->arguments.silent)
		printf("Total time %.9lfs, comparing time %.9lfs with %d workers\n", max_total_whole_time, max_total_comparison_time, this->arguments.workers);
	levdist_finalize(this);
	
	return 0;
	
}

int levdist_list_files_in_args(levdist_t* this, int argc, char* argv[])
{
	// Traverse all arguments
	for ( int current = 1; current < argc; ++current )
	{
		// Skip command-line options
		const char* arg = argv[current];
		if ( *arg == '-' )
			continue;
		//Fills the queue with the files to be compared
		dir_list_files_in_dir(this->files, arg, this->arguments.recursive);
	}

	return 0;
}

void levdist_print_and_destroy_files(levdist_t* this)
{
	long count = 0;
	while ( ! queue_is_empty(this->files) )
	{
		char* filename = (char*)queue_pop(this->files);
		printf("%ld: %s\n", ++count, filename);
		free(filename);
	}
}

void levdist_print_files(levdist_t* this)
{
	long count = 0;
	for ( queue_iterator_t itr = queue_begin(this->files); itr != queue_end(this->files); itr = queue_next(itr) )
	{
		const char* filename = (const char*)queue_data(itr);
		printf("%ld: %s\n", ++count, filename);
	}
}

unsigned int levdist_combinations(unsigned int n)
{
	unsigned int results = 1;
	for(unsigned int i = n; i > n-2; --i)
		results *= i;
	results /= 2;
	return results;
}

void levdist_finalize(levdist_t* this)
{
	//frees memory
	if(this->files)
		queue_destroy(this->files, true);
	if(this->results)
		array_destroy(this->results);
}

int levdist_world_size()
{
	int comm_sz = -1;
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	return comm_sz;
}

int levdist_my_rank()
{
	int comm_rank = -1;
	MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
	return comm_rank;
}

comparison_t* levdist_list_comparisons(queue_t* queue)
{
	//creates an array of comparisons with all possible combinations between files
	comparison_t* comparison_array = (comparison_t*) calloc(levdist_combinations(queue_count(queue)), sizeof(comparison_t));
	size_t current_comparison = 0;
	assert(comparison_array);
	
	for(queue_iterator_t first_element = queue_begin(queue), second_element = first_element; !queue_is_empty(queue) && second_element != NULL; second_element = queue_next(second_element)){
		if(second_element == queue_begin(queue))
			continue;
		//clears the char strings
		memset(comparison_array[current_comparison].file_1, '\0', FILENAME_MAX);
		memset(comparison_array[current_comparison].file_2, '\0', FILENAME_MAX);
		
		//copies each pair of files into the strings of the array
		strcpy(comparison_array[current_comparison].file_1, (const char*) queue_data(first_element));
		strcpy(comparison_array[current_comparison].file_2, (const char*) queue_data(second_element));
		
		++current_comparison;
		
		if(queue_next(second_element) == NULL){
			free(queue_pop(queue));
			second_element = queue_begin(queue); //the for cycle increments it to the next node ahead the head
			first_element = queue_begin(queue); //pop the head
			if(second_element == NULL)
				break;
		}
	}
	
	return comparison_array;
}

int* levdist_distribute_load(int comparisons, int nprocs)
{
	int start = 0;
	int end = 0;
	int remainder = 0;
	int quotient = 0;
	quotient = comparisons / nprocs;
	remainder = comparisons % nprocs;
	
	int* work_distribution = (int*) calloc(nprocs, sizeof(int));
	
	for(int index = 0; index < nprocs; ++index){
		start = index * quotient + min(index, remainder);
		end = (index + 1) * quotient + min((index + 1), remainder);
		work_distribution[index] = end - start;
	}
	
	return work_distribution;
}

int* levdist_compute_displacements(int* work_distribution)
{
	int nprocs = levdist_world_size();
	
	//array of memory offsets for each position of comparisons array
	int* displacements = (int*) calloc(nprocs, sizeof(int));
	
	for(int index = 0; index < nprocs; ++index){
		if(index == 0){
			displacements[index] = 0;
		}else{
			displacements[index] = work_distribution[index - 1] * sizeof(comparison_t) + displacements[index - 1];
		}
	}
	return displacements;
}

void levdist_init_results(result_t* results, int result_count)
{
	for(int index = 0; index < result_count; ++index)
		memset(results[index].result, '\0', RESULT_MAX);
}
