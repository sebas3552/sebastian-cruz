**Diseño del proyecto Distancia de Levenshtein usando MPI**

Para explicar la decisión tomada con respecto al esquema de distribución de trabajo, considere la siguiente tabla comparativa:

|Esquema | Comunicación| Poder computacional|
|--------|-------------|--------------------|
|Estático|    Poca     | Puede desaprovecharse si la carga asignada a un proceso es muy poca en comparación con la carga asignada a los demás|
|--------|-------------|---------------------|
|Dinámico|   Mucha     | Se aprovecha al máximo ya que cada proceso se mantiene ocupado solicitando un nuevo trabajo cuando finaliza trabajo asignado previamente|

Como la comunicación suele ser aún más costosa -en términos de tiempo- entre procesos, se optó por la implementación de un sistema de distribución estático por bloques, donde se asigna a cada proceso, incluido el principal, una cantidad de trabajo aproximada de num_comparaciones / num_procesos, utilizando una distribución equitativa del trabajo. Aunque también existe el sistema híbrido que combina la distribución por bloques con la cíclica, no se consideró por la complejidad que significaba para la implementación en este proyecto.

Algoritmo en pseudocódigo para el proyecto:

~~~
si rango == 0
    procesar argumentos
    listar archivos
    enviar cantidad de archivos a cada proceso
    crear estructura de datos con las comparaciones a realizar
    calcular la cantidad de elementos que corresponde a cada proceso
    repartir las comparaciones entre procesos
    mientras hayan comparaciones (asignadas al proceso 0) hacer
        calcular distancia de levenshtein entre los archivos
        meter los resultados al arreglo de resultados
    recibir los resultados de los procesos
    combinar los resultados del proceso 0 con los resultados de los demás procesos
    ordenar arreglo de resultados totales
    imprimir resultados
sino
    recibir cantidad de archivos del proceso 0
    recibir del proceso 0 las comparaciones correspondientes
    mientras hayan comparaciones hacer
        calcular distancia de levenshtein entre los archivos
        meter los resultados al arreglo de resultados
    enviar resultados al proceso 0
~~~

Note que el proceso principal es el encargado de realizar toda la logística de análisis de argumentos, levantado del listado de archivos, manejo de la estructura de datos para las comparaciones y sus resultados, distribución de la carga de trabajo entre los procesos, envío y recepción de datos hacia y desde los demás procesos, ordenamiento e impresión de resultados finales, así como también debe realizar trabajo correspondiente a comparaciones. Este diseño se basa en la simplicidad, dado que siempre existe al menos el proceso principal, entonces en ese caso solo el proceso principal llevaría a cabo todo el trabajo. En el caso que existan varios procesos, sería poco útil habilitar al proceso principal solo para realizar tareas de logística ya que pasará ocioso gran parte del tiempo.