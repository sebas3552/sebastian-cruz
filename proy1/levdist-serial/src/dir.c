#include <assert.h>
#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wchar.h>

#include "dir.h"
#define READ "r"

int dir_list_files_in_dir(queue_t* queue, const char* path, bool recursive)
{
	assert(queue);
	assert(path);
	assert(*path);

	// Try to open the directory
	DIR* dir = opendir(path);
	if ( dir == NULL ){
		struct stat fileStat;
		// Checks if path is a file
		if(stat(path, &fileStat) < 0)
			return fprintf(stderr, "levdist: error: could not open directory or file \"%s\"\n", path), 1;
		else
			return dir_list_file(queue, path);
	}

	// Load the directory entries (contents) one by one
	struct dirent* entry;
	while ( (entry = readdir(dir)) != NULL )
	{
		// If the entry is a directory
		if ( entry->d_type == DT_DIR )
		{
			// Ignore hidden files and directories
			if ( *entry->d_name == '.' )
				continue;

			// If files should be listed recursively
			if ( recursive )
			{
				// Concatenate the directory to the path
				char relative_path[PATH_MAX];
				sprintf(relative_path, "%s/%s", path, entry->d_name);

				// Load files in the subdirectory
				dir_list_files_in_dir(queue, relative_path, recursive);
			}
		}
		else // The entry is not a directory
		{
			// Concatenate the directory to the path
			char relative_path[PATH_MAX];
			sprintf(relative_path, "%s/%s", path, entry->d_name);
			// Append the file or symlink to the queue
			queue_append(queue, strdup(relative_path));
		}
	}

	// Sucess
	closedir(dir);
	return 0;
}

int dir_list_file(queue_t* queue, const char *file_name)
{
	assert(queue);
	assert(file_name);
	assert(*file_name);
	queue_append(queue, strdup(file_name));
	return 0;
}

char* dir_load_file(const char* file_name)
{
	assert(file_name);
	FILE* file = fopen(file_name, READ);
	if(file == NULL){
		fprintf(stderr, "Error! file \"%s\" could not be loaded.\n", file_name);
		return 0;
	}
	char string[PATH_MAX];
	char *all_data = (char*)calloc(1, sizeof(char));
	while(fgets(string, PATH_MAX, file)){
		all_data = (char*)realloc(all_data, strlen(all_data) + strlen(string)+1);
		strcat(all_data, string);
	}
	fclose(file);
	return all_data;
}

wchar_t* dir_load_file_as_unicode(const char* file_name)
{
	assert(file_name);
	FILE* file = fopen(file_name, READ);
	if(file == NULL){
		fprintf(stderr, "Error! file \"%s\" could not be loaded.\n", file_name);
		return 0;
	}
	struct stat file_stat;
	stat(file_name, &file_stat); //obtains data from file
	wchar_t *all_data = (wchar_t*)calloc(file_stat.st_size+1, sizeof(wchar_t)); //allocates memory for the file
	wchar_t readed;
	for(int i = 0; (readed = getwc(file)) != (wchar_t)WEOF; ++i){
		all_data[i] = readed; //appends characters to the single-string file
	}
	fclose(file);
	return all_data;
}
