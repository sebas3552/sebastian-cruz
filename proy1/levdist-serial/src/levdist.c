#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "concurrency.h"
#include "dir.h"
#include "levdist.h"
#include "levenshtein.h"

// Private functions:

/// Shows how to travese the list removing elements
void levdist_print_and_destroy_files(levdist_t* this);

/// Shows how to traverse the list without removing elements
void levdist_print_files(levdist_t* this);


void levdist_init(levdist_t* this)
{
	arguments_init(&this->arguments);
	this->files = NULL;
	this->results = NULL;
}

int levdist_run(levdist_t* this, int argc, char* argv[])
{
	// Analyze the arguments given by the user
	this->arguments = arguments_analyze(argc, argv);

	// If arguments are incorrect, stop
	if ( this->arguments.error )
		return this->arguments.error;

	// If user asked for help or software version, print it and stop
	if ( this->arguments.help_asked )
		return arguments_print_usage();
	if ( this->arguments.version_asked )
		return arguments_print_version();

	// If user did not provided directories, stop
	if ( this->arguments.dir_count <= 0 )
		return fprintf(stderr, "levdist: error: no directories given\n"), 1;

	// Arguments seems fine, process the directories
	return levdist_process_dirs(this, argc, argv);
}

int levdist_process_dirs(levdist_t* this, int argc, char* argv[])
{
	// Start counting the time
	walltime_t start;
	walltime_start(&start);

	// Load all files into a list
	this->files = queue_create();
	levdist_list_files_in_args(this, argc, argv);
	// If queue has just one file, error
	if(queue_count(this->files) < 2){
		fprintf(stderr, "levdist: error: at least two files are required to compare\n");
		levdist_finalize(this);
		return -1;
	}
	queue_sort(this->files); //sort aphabetically first
	int queue_size = queue_count(this->files);
	unsigned long long combinations = 0;
	combinations = levdist_combinations(queue_size);
	this->results = array_create(combinations);
	if(!this->results){
		fprintf(stderr, "An error ocurred, no more memory available!\n");
		levdist_finalize(this);
		return -1;
	}
	char result[PATH_MAX];
	// Pair of files for ASCII and UNICODE options (ASCII default)
	char* file_1;
	char* file_2;
	wchar_t* unicode_file_1;
	wchar_t* unicode_file_2;
	int distance = 0;
	//The head of the queue is compared with all other elements in queue, and then is popped off.
		for(queue_iterator_t i = queue_begin(this->files), j = i; !queue_is_empty(this->files) && j != NULL; j = queue_next(j)){
			if(j == queue_begin(this->files))
				continue;
			if(!strcmp((char*)queue_data(i), (char*)queue_data(j))){ //if the file names are the same, error
				fprintf(stderr, "levdist: error: at least two files are required to compare\n");
				levdist_finalize(this);
				return -1;
			}
			//Switches between comparisons in ASCII or UNICODE mode.
			if(this->arguments.unicode){
				unicode_file_1 = dir_load_file_as_unicode((char*) queue_data(i));
				unicode_file_2 = dir_load_file_as_unicode((char*) queue_data(j));
				if(!unicode_file_1 || !unicode_file_2){ //if files couldn't be loaded, return
					levdist_finalize(this); 
					return -1;	
				}
				distance = levenshtein_distance_unicode(unicode_file_1, unicode_file_2); //distance with unicode characters
				free(unicode_file_1);
				free(unicode_file_2);
			}else{
				file_1 = dir_load_file((char*) queue_data(i));
				file_2 = dir_load_file((char*) queue_data(j));
				if(!file_1 || !file_2){ //if files couldn't be loaded, return
					levdist_finalize(this);
					return -1;	
				}
				distance = levenshtein_distance(file_1, file_2); //distance with ASCII characters
				free(file_1);
				free(file_2);
			}
			if(distance == -1){
				levdist_finalize(this);
				return -1;
			}
			sprintf(result, "%d\t%s\t%s", (int) distance, (char*) queue_data(i),(char*) queue_data(j)); //compares the head node with all others in queue
			//array stores the results to be displayed on screen.
			array_push(this->results, strdup(result));
			if(queue_next(j) == NULL){
				free(queue_pop(this->files));
				j = queue_begin(this->files); //the for cycle increments it to the next node ahead the head
				i = queue_begin(this->files); //pop the head
				if(j == NULL)
					break;
			}
		}
	
	//Sorts the results first by distance and then alphabetically if some distances are repeated
	array_qsort_by_distance(this->results->data, 0, array_length(this->results)-1);
	array_sort_by_parts(this->results);
	
	for(int i = 0; i < (int)array_length(this->results); ++i){
		puts(this->results->data[i]);
	}
	levdist_finalize(this);
	// Report elapsed time
	if(!this->arguments.quiet && !this->arguments.silent)
		printf("Elapsed %.9lfs\n", walltime_elapsed(&start));
	return 0;
}

int levdist_list_files_in_args(levdist_t* this, int argc, char* argv[])
{
	// Traverse all arguments
	for ( int current = 1; current < argc; ++current )
	{
		// Skip command-line options
		const char* arg = argv[current];
		if ( *arg == '-' )
			continue;
		//Fills the queue with the files to be compared
		dir_list_files_in_dir(this->files, arg, this->arguments.recursive);
	}

	return 0;
}

void levdist_print_and_destroy_files(levdist_t* this)
{
	long count = 0;
	while ( ! queue_is_empty(this->files) )
	{
		char* filename = (char*)queue_pop(this->files);
		printf("%ld: %s\n", ++count, filename);
		free(filename);
	}
}

void levdist_print_files(levdist_t* this)
{
	long count = 0;
	for ( queue_iterator_t itr = queue_begin(this->files); itr != queue_end(this->files); itr = queue_next(itr) )
	{
		const char* filename = (const char*)queue_data(itr);
		printf("%ld: %s\n", ++count, filename);
	}
}

unsigned int levdist_combinations(unsigned int n)
{
	unsigned int results = 1;
	for(unsigned int i = n; i > n-2; --i)
		results *= i;
	results /= 2;
	return results;
}

void levdist_finalize(levdist_t* this)
{
	//frees memory
	if(this->files)
		queue_destroy(this->files, true);
	if(this->results)
		array_destroy(this->results);
}
