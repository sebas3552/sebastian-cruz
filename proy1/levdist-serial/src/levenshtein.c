#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include "levenshtein.h"

int levenshtein_distance(const char* string_1, const char* string_2)
{
	unsigned long long rows = 0;
	unsigned long long columns = 0;
	int** matrix;
	int cost = 0;
	int distance = 0;

    rows = (unsigned long long)strlen(string_1);
    columns = (unsigned long long)strlen(string_2);

    if(!rows) 
		return columns;
    if(!columns)
		return rows;
	//adjust size of matrix to allocate an extra row and an extra column
	++rows;  
	++columns;
	//allocates the matrix in heap
    matrix = (int**) calloc(rows, sizeof(int*));
	matrix[0] = (int*) calloc(columns, sizeof(int));
	matrix[0][0] = 0; //initializes the uppermost left cell to 0
	
    if (!matrix){
		fprintf(stderr, "An error ocurred, not enough space for Levenshtein's matrix!\n");
		return -1;
	}
	//initializes the matrix as levenshtein's algorithm requires
	for(int i = 0; i < (int)columns; ++i)
		matrix[0][i] = i;

// Fills the matrix with values that depends on three other cells
    for(size_t i = 1; i < rows; i++){
		matrix[i] = (int*) calloc(columns, sizeof(int)); //creates the rows and columns it needs for calculation in execution time
		for(size_t j = 1; j < columns; j++){ 	
			matrix[i][0] = i;
			cost = (string_1[i-1] == string_2[j-1]? 0 : 1);
			matrix[i][j] = min(min(matrix[i-1][j]+1, matrix[i][j-1]+1), matrix[i-1][j-1]+cost);
		} 
		free(matrix[i-1]);  //then frees the previous row that is not needed anymore, to save space
	}
// Returns the matrix final corner which contains the final distance

	distance = matrix[rows-1][columns-1];
	free(matrix[rows-1]);
    free(matrix);
    return distance;
}

int levenshtein_distance_unicode(const wchar_t* string_1, const wchar_t* string_2)
{
	unsigned long long rows = 0;
	unsigned long long columns = 0;
	int** matrix;
	int cost = 0;
	int distance = 0;

    rows = (unsigned long long)wcslen(string_1);
    columns = (unsigned long long)wcslen(string_2);

    if(!rows) 
		return columns;
    if(!columns)
		return rows;
	//adjust size of matrix to allocate an extra row and an extra column
	++rows;  
	++columns;
	//allocates the matrix in heap
    matrix = (int**) calloc(rows, sizeof(int*));
    for(size_t j = 0; j < rows; ++j)
		matrix[j] = (int*) calloc(columns, sizeof(int));
	
    if (!matrix){
		fprintf(stderr, "An error ocurred, not enough space for Levenshtein's matrix!\n");
		return -1;
	}
	//initializes the matrix as levenshtein's algorithm requires
	levenshtein_init_matrix(matrix, rows, columns);

// Fills the matrix with values that depends on three other cells
    for(size_t i = 1; i < rows; i++){
		for(size_t j = 1; j < columns; j++){ 
			cost = (string_1[i-1] == string_2[j-1]? 0 : 1);
			matrix[i][j] = min(min(matrix[i-1][j]+1, matrix[i][j-1]+1), matrix[i-1][j-1]+cost);
		} 
	}
// Returns the matrix final corner which contains the final distance

	distance = matrix[rows-1][columns-1];
	for(size_t i = 0; i < rows; ++i)
		free(matrix[i]);
    free(matrix);
    return distance;
}


int min(int num_1, int num_2)
{
	return(num_1 < num_2? num_1 : num_2);
}

void levenshtein_init_matrix(int** matrix, int rows, int columns)
{
	assert(matrix);
	for(int i = 0; i < rows; ++i){
		matrix[i][0] = i;
	}
	for(int j = 0; j < columns; ++j){
		matrix[0][j] = j;
	}
}
