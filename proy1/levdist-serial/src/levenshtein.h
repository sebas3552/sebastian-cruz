/**
 * @brief Main method to calculate the levenshtein distance between two strings Original source code taken from: https://es.wikipedia.org/wiki/Distancia_de_Levenshtein.
 * @param string_1 The first string to be compared.
 * @param string_2 The second string to be compared.
 * @return The distance between the two strings calculated by levenshtein's algorithm.
 */
int levenshtein_distance(const char* string_1, const char* string_2);

/**
 * @brief Main method to calculate the levenshtein distance between two strings in Unicode format.
 * @param string_1 The first string to be compared.
 * @param string_2 The second string to be compared.
 * @return The distance between the two strings calculated by levenshtein's algorithm.
 */
int levenshtein_distance_unicode(const wchar_t* string_1, const wchar_t* string_2);

/// Function that initializes a matrix or a vector with the first numbers needed by Levenshtein's algorithm.
void levenshtein_init_matrix(int** matrix, int rows, int columns);

/// Function that gets the smallest value from two numbers.
int min(int num_1, int num_2);
