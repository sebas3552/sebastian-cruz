# Levenshtein Distance

This program finds most similar files stored in directories using the Levenshtein Distance algorithm by comparing each line of each single file against all the files left, regardless of the type of file.

This algorithm is useful for important research areas such as computer science (in the field of Artificial Intelligence), information theory (such as error-control techniques), and biology (such as DNA chains comparisons), but also is widely used in daily common tasks, like *spell-checkers* and *word completion* features in web browsers, electronic forms, and much more.

Note: the original source code for Levenshtein's algorithm was taken from: *https://es.wikipedia.org/wiki/Distancia`_`de_Levenshtein*
and modified for more readability and efficiency in memory usage by Sebastián Cruz.


## User manual

There are a few commands to execute this program as needed. The general command form is:

~~~
 $ levdist [-qr][-w W] DIRS|FILES
~~~

The commands enclosed in square brackets are optional. Then, DIRS|FILES is the path of a directory which has the files to be compared. This directory could also have more directories inside it, but the program will ignore them if the --recursive command has not been set.

Here is a list that explains each optional command:

* `-q` or `--quiet`: do not print elapsed time.
* `-Q` or `--silent`: do not generate output at all.
* `-r` or `--recursive`: analyze files in subdirectories.
* `-w W`: use W workers to accomplish the task.
* `-u`: read the files to be compared with Unicode charset.

## Building

Requisites:

1. A POSIX Unix operating system
2. A GCC-compatible compiler


####Steps to build:
In a terminal, invoke this program's Makefile by entering `$ make [options]`, with any or one of the following building options:

* `debug`: Generates an executable `levdist` with debugging options enabled.
* `release`: Generates an executable with maximum optimizations, and without debugging options.
* `doc`: Generates documentation files for the project with Doxygen. You must have this documentation manager installed to invoke `make` with this option.
* `test`: Tests the executable against black-box test cases.
* `all`: Equals to `make debug doc test`.
* `memcheck`: Searches for memory leaks running the test cases given.
* `clean`: Removes auto-generated files created by `Makefile`.
* `install`: Copies the executable file into `~/bin` dir.
* `uninstall`: Removes the executable file from `~/bin`.

Note: if any option is set, then the option set by default is `debug`.

####Installing and uninstalling the program:
To properly install this software in your computer, just enter the command `$make install`. It will create a copy of the executable in your home directory, ready to use.

Now, to uninstall the program, just enter the command `$make uninstall`, and it will remove the executable copied at install.

## Testing
To make testing, compile the project with `test` option enabled. Automatically, the program will invoke `levdist` with debugging options and other commands read from "args.txt" files over the files to be compared found in each subdirectory of *test*. Then, at backstage it will create its own test directory at `bin` to put there the results of each test case, and then it will compare the *output* and *error* text files generated with the ones present in each test case, to verify if they look the same. If not, the output at terminal will inform about the differences between test cases. Finally, the program will automatically remove the temporary files it created through testing process.


## Authors:

Sebastián Cruz Chavarria, email: cruzchavarria925@hotmail.com

This software has been developed **only** for academic purposes as one of the projects of a high-level programming course, at Universidad de Costa Rica.


MIT License

Copyright (c) 2018 Sebastián Cruz.
