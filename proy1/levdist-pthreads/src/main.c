#include "levdist.h"
#include <locale.h>

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "");
	// Create a controller "object"
	levdist_t levdist;
	levdist_init(&levdist);
	// Run the command
	return levdist_run(&levdist, argc, argv);
}
