#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include "levenshtein.h"
#define SEM_RED 0
#define SEM_GREEN 1
#define SIGMA_ASCII 256
#define SIGMA_UNICODE 65536

static size_t current_i = 0;
static pthread_barrier_t row_barrier;
static unsigned long long** x;
static unsigned long long** d;

typedef struct ascii_data
{
	const unsigned char* p;
	const unsigned char* t;
	size_t rank;
	size_t m;
	size_t n;
	size_t worker_count;
	unsigned char* q;
}ascii_data_t;

typedef struct unicode_data
{
	const wchar_t* p;
	const wchar_t* t;
	size_t rank;
	size_t m;
	size_t n;
	size_t worker_count;
	wchar_t* q;
}unicode_data_t;

size_t levenshtein_distance_ascii(const char* string_1, const char* string_2, size_t requested_workers)
{
	size_t worker_count = requested_workers;
	ascii_data_t* workers_data = (ascii_data_t*) calloc(worker_count, sizeof(ascii_data_t));
	size_t string_1_length = strlen(string_1);
	size_t string_2_length = strlen(string_2);
	if(string_1_length < worker_count || string_2_length < worker_count){
		worker_count = string_1_length < string_2_length? string_1_length : string_2_length;
		if(worker_count == 0)
			++worker_count;
	}
	unsigned char alphabet[SIGMA_ASCII];
	levenshtein_init_ascii_alphabet(&alphabet[0]);
	//data collection for every single thread, to avoid static variables
	for(size_t index = 0; index < worker_count; ++index){
		workers_data[index].p = (unsigned char *)string_1;
		workers_data[index].t = (unsigned char *)string_2;
		workers_data[index].m = string_1_length;
		workers_data[index].n = string_2_length;
		workers_data[index].q = &alphabet[0];
		workers_data[index].rank = index;
		workers_data[index].worker_count = worker_count;
	}
	pthread_t* workers = (pthread_t*) calloc(worker_count, sizeof(pthread_t));
	assert(workers);

	x = (unsigned long long**) calloc(SIGMA_ASCII, sizeof(unsigned long long*));
	assert(x);
	for(size_t j = 0; j < SIGMA_ASCII; ++j){
		x[j] = (unsigned long long*) calloc(string_2_length+1, sizeof(unsigned long long*));
		assert(x[j]);
	}
	//parallel initialization of matrix x by rows
	for(size_t index = 0; index < worker_count; ++index){
		pthread_create(&workers[index], NULL, levenshtein_init_matrix_x_ascii, (void*) &workers_data[index]);
	}
	
	//end for parallel
	for(size_t index = 0; index < worker_count; ++index)
		pthread_join(workers[index], NULL);
	
	d = (unsigned long long**) calloc(string_1_length+1, sizeof(unsigned long long*));
	d[0] = (unsigned long long*) calloc(string_2_length+1, sizeof(unsigned long long*));
	pthread_barrier_init(&row_barrier, NULL, worker_count);
	current_i = 0;
	for(size_t index = 0; index < worker_count; ++index)
		pthread_create(&workers[index], NULL, levenshtein_parallel_distance_ascii, (void*) &workers_data[index]);

	for(size_t index = 0; index < worker_count; ++index)
		pthread_join(workers[index], NULL);

	assert(current_i > 0);
	free(workers);
	free(workers_data);

	pthread_barrier_destroy(&row_barrier);
	for(size_t index = 0; index < SIGMA_ASCII; ++index)
		free(x[index]);
	free(x);
	size_t distance = d[string_1_length][string_2_length];
	free(d[string_1_length]);
	if(string_1_length > 0)
		free(d[string_1_length-1]);
	free(d);
	return distance;
}

size_t levenshtein_distance_unicode(const wchar_t* string_1, const wchar_t* string_2, size_t requested_workers)
{
	size_t worker_count = requested_workers;
	unicode_data_t* workers_data = (unicode_data_t*) calloc(worker_count, sizeof(unicode_data_t));
	size_t string_1_length = wcslen(string_1);
	size_t string_2_length = wcslen(string_2);
	if(string_1_length < worker_count || string_2_length < worker_count){
		worker_count = string_1_length < string_2_length? string_1_length : string_2_length;
		if(worker_count == 0)
			++worker_count;
	}
	wchar_t alphabet[SIGMA_UNICODE];
	levenshtein_init_unicode_alphabet(&alphabet[0]);
	//data collection for every single thread, to avoid static variables
	for(size_t index = 0; index < worker_count; ++index){
		workers_data[index].p = string_1;
		workers_data[index].t = string_2;
		workers_data[index].m = string_1_length;
		workers_data[index].n = string_2_length;
		workers_data[index].q = &alphabet[0];
		workers_data[index].rank = index;
		workers_data[index].worker_count = worker_count;
	}
	pthread_t* workers = (pthread_t*) calloc(worker_count, sizeof(pthread_t));
	assert(workers);
	x = (unsigned long long**) calloc(SIGMA_UNICODE, sizeof(unsigned long long*));
	assert(x);
	for(size_t j = 0; j < SIGMA_UNICODE; ++j){
		x[j] = (unsigned long long*) calloc(string_2_length+1, sizeof(unsigned long long*));
		assert(x[j]);
	}

	//parallel initialization of matrix x by rows
	for(size_t index = 0; index < worker_count; ++index){
		pthread_create(&workers[index], NULL, levenshtein_init_matrix_x_unicode, (void*) &workers_data[index]);
	}
	
	//end for parallel
	for(size_t index = 0; index < worker_count; ++index)
		pthread_join(workers[index], NULL);
	
	d = (unsigned long long**) calloc(string_1_length+1, sizeof(unsigned long long*));
	d[0] = (unsigned long long*) calloc(string_2_length+1, sizeof(unsigned long long*));
	pthread_barrier_init(&row_barrier, NULL, worker_count);
	current_i = 0;
	//send threads to fill matrix d in "parallel"
	for(size_t index = 0; index < worker_count; ++index)
		pthread_create(&workers[index], NULL, levenshtein_parallel_distance_unicode, (void*) &workers_data[index]);

	for(size_t index = 0; index < worker_count; ++index)
		pthread_join(workers[index], NULL);

	assert(current_i > 0);
	free(workers);
	free(workers_data);
	pthread_barrier_destroy(&row_barrier);
	for(size_t index = 0; index < SIGMA_UNICODE; ++index)
		free(x[index]);
	free(x);
	size_t distance = d[string_1_length][string_2_length];
	free(d[string_1_length]);
	if(string_1_length > 0)
		free(d[string_1_length-1]);
	free(d);
	return distance;
}

void* levenshtein_init_matrix_x_ascii(void* data)
{
	const ascii_data_t* my_data = (const ascii_data_t*) data;
	size_t rank = my_data->rank;
	size_t my_first_i = rank*(SIGMA_ASCII/my_data->worker_count);
	size_t my_last_i = rank == my_data->worker_count-1? SIGMA_ASCII : my_first_i + ceil((double)SIGMA_ASCII/(double)my_data->worker_count);
	if(my_last_i > SIGMA_ASCII)
		my_last_i = SIGMA_ASCII;
	
	for(size_t i = my_first_i; i < my_last_i; ++i){	
		for(size_t j = 0; j < my_data->n+1; ++j){
			if(j == 0){
				x[i][j] = 0;
			}else{
				if(my_data->t[j-1] == my_data->q[i]){
					x[i][j] = j;
				}else{
					x[i][j] = x[i][j-1];
				}
			}
		}
	}
	return NULL;
}

void* levenshtein_init_matrix_x_unicode(void* data)
{
	const unicode_data_t* my_data = (const unicode_data_t*) data;
	size_t rank = my_data->rank;
	size_t my_first_i = rank*(SIGMA_UNICODE/my_data->worker_count);
	size_t my_last_i = rank == my_data->worker_count-1? SIGMA_UNICODE : my_first_i + ceil((double)SIGMA_UNICODE/(double)my_data->worker_count);
	if(my_last_i > SIGMA_UNICODE)
		my_last_i = SIGMA_UNICODE;
	
	for(size_t i = my_first_i; i < my_last_i; ++i){	
		for(size_t j = 0; j < my_data->n+1; ++j){
			if(j == 0){
				x[i][j] = 0;
			}else{
				if(my_data->t[j-1] == my_data->q[i]){
					x[i][j] = j;
				}else{
					x[i][j] = x[i][j-1];
				}
			}
		}
	}
	return NULL;
}

void* levenshtein_parallel_distance_ascii(void* data)
{
	const ascii_data_t* my_data = (const ascii_data_t*) data;
	size_t rank = my_data->rank;
	size_t my_first_j = rank*(my_data->n/my_data->worker_count);
	size_t my_last_j = rank == my_data->worker_count -1? my_data->n+1 : my_first_j + ceil((double)my_data->n/(double)my_data->worker_count);
	while(current_i < my_data->m+1){
		//thread 0 is in charge of allocating and freeing unnecessary rows
		if(rank == 0 && current_i >= 1){
			d[current_i] = (unsigned long long*) calloc(my_data->n+1, sizeof(unsigned long long*));
		}
		if(rank == 0 && current_i >= 2){
			free(d[current_i-2]);
		}
		pthread_barrier_wait(&row_barrier); //wait until first thread had requested memory for d[current_i]
		
		for(size_t j = my_first_j; j < my_last_j; ++j){
			if(current_i == 0){	
				d[current_i][j] = j;
			}else{
				if(j == 0){
					d[current_i][j] = current_i;
				}else{
					if(my_data->t[j-1] == my_data->p[current_i-1]){
						d[current_i][j] = d[current_i-1][j-1];
					}else{
						assert(current_i-1 < my_data->m+1);
						assert(j < my_data->n+1);
						if(j == my_first_j){
							if(x[my_data->p[current_i-1]][j] == 0){
								d[current_i][j] = 1 + (( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) < current_i+j-1? ( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) : current_i+j-1);
							}else{
								d[current_i][j] = 1 + (( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) < (d[current_i-1][x[my_data->p[current_i-1]][j]-1] + (j-1-x[my_data->p[current_i-1]][j]))? ( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) : (d[current_i-1][x[my_data->p[current_i-1]][j]-1] + (j-1-x[my_data->p[current_i-1]][j])));
							}
						}else{
							d[current_i][j] = 1 + (d[current_i-1][j-1] < d[current_i-1][j]? (d[current_i-1][j-1] < d[current_i][j-1]? d[current_i-1][j-1] : d[current_i][j-1]) : (d[current_i-1][j] < d[current_i][j-1]? d[current_i-1][j] : d[current_i][j-1]));
						}
					}
				}
			}
		}
		pthread_barrier_wait(&row_barrier); //remain waiting until all threads had calculated the whole row
		if(rank == 0) //"main" thread increments row
			++current_i; 
		pthread_barrier_wait(&row_barrier); //wait again so that every thread has the variable current_i updated
	}
	return NULL;
}

void* levenshtein_parallel_distance_unicode(void* data)
{
	const unicode_data_t* my_data = (const unicode_data_t*) data;
	size_t rank = my_data->rank;
	size_t my_first_j = rank*(my_data->n/my_data->worker_count);
	size_t my_last_j = rank == my_data->worker_count -1? my_data->n+1 : my_first_j + ceil((double)my_data->n/(double)my_data->worker_count);
	while(current_i < my_data->m+1){
		
		if(rank == 0 && current_i >= 1){
			d[current_i] = (unsigned long long*) calloc(my_data->n+1, sizeof(unsigned long long*));
		}
		if(rank == 0 && current_i >= 2){
			free(d[current_i-2]);
		}
		pthread_barrier_wait(&row_barrier);
		
		for(size_t j = my_first_j; j < my_last_j; ++j){
			if(current_i == 0){	
				d[current_i][j] = j;
			}else{
				if(j == 0){
					d[current_i][j] = current_i;
				}else{
					if(my_data->t[j-1] == my_data->p[current_i-1]){
						d[current_i][j] = d[current_i-1][j-1];
					}else{
						assert(current_i-1 < my_data->m+1);
						assert(j < my_data->n+1);
						if(j == my_first_j){
							if(x[my_data->p[current_i-1]][j] == 0){
								d[current_i][j] = 1 + (( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) < current_i+j-1? ( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) : current_i+j-1);
							}else{
								d[current_i][j] = 1 + (( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) < (d[current_i-1][x[my_data->p[current_i-1]][j]-1] + (j-1-x[my_data->p[current_i-1]][j]))? ( d[current_i-1][j] < d[current_i-1][j-1]? d[current_i-1][j] : d[current_i-1][j-1]) : (d[current_i-1][x[my_data->p[current_i-1]][j]-1] + (j-1-x[my_data->p[current_i-1]][j])));
							}
						}else{
							d[current_i][j] = 1 + (d[current_i-1][j-1] < d[current_i-1][j]? (d[current_i-1][j-1] < d[current_i][j-1]? d[current_i-1][j-1] : d[current_i][j-1]) : (d[current_i-1][j] < d[current_i][j-1]? d[current_i-1][j] : d[current_i][j-1]));
						}
					}
				}
			}
		}
		pthread_barrier_wait(&row_barrier);
		if(rank == 0)
			++current_i;
		pthread_barrier_wait(&row_barrier);
	}
	return NULL;
}

void levenshtein_init_ascii_alphabet(unsigned char* alphabet)
{
	for(size_t i = 0; i < SIGMA_ASCII; ++i){
		alphabet[i] = (unsigned char) i; //fills the alphabet with standard ASCII charset
	}
}

void levenshtein_init_unicode_alphabet(wchar_t* alphabet)
{
	for(size_t i = 0; i < SIGMA_UNICODE; ++i){
		alphabet[i] = (wchar_t) i; //fills the alphabet with standard UNICODE charset
		assert((wchar_t) i < SIGMA_UNICODE);
	}
}
