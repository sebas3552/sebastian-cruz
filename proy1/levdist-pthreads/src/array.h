#ifndef ARRAY_H
#define ARRAY_H
#include <stdlib.h>

/// A struct that contains a fixed-size array of character strings, its size and the number of the last item inserted.
typedef struct array{
	char** data;
	size_t max_size;
	size_t current_element;
} array_t;

/**
 * @brief Create an empty array
 * @param size Size desired for the array.
 * @return Pointer to the new created array in heap memory.
 * @remarks The pointed array must be freed with @a array_destroy().
 */
array_t* array_create(size_t size);

/// Returns the size of the given array in argument.
size_t array_length(array_t* array);

/// Frees the memory needed by victim, and frees also all its content.
void array_destroy(array_t* victim);

/** 
 * @brief Appends an element (character string) to the array.
 * @param array Array in which an element is going to be appended.
 * @param string String to be appended to array.
 * @return 0 if the elements count did not exceed the array's capacity, 1 otherwise.
 */
int array_push(array_t* array, char* string);

/**
 * @brief Function that sorts an array of character strings using quicksort algorithm.
 * @param data Array of strings to be sorted.
 * @param start Starting point of the array to be sorted.
 * @param end Ending point of the array to be sorted.
 */
void array_qsort(char** data, int start, int end);

/**
 * @brief "Public" function to sort an array.
 * @param array Pointer to the struct array_t to be sorted.
 * @return a pointer to the sorted struct array_t.
 */
 
array_t* array_sort(array_t* array);

/**
 * @brief Function that sorts an array only by comparing the distance printed in each string.
 * @param data Array of strings to be sorted.
 * @param start Sarting point of the array to be sorted.
 * @param end Ending point of the array to be sorted.
 */
void array_qsort_by_distance(char** data, int start, int end);

/**
 * @brief Function that searches for contiguous strings in data whose distance numbers are the same.
 * @param array Struct array whose data is going to be sorted, if contiguous strings with the same distance number are found. 
 */
void array_sort_by_parts(array_t* array);

/**
 * @brief Function that sorts an interval of strings with the same distance number alphabetically.
 * @param sub_array Sub array to be sorted.
 * @param start Starting point of the array to be sorted.
 * @param end Ending point of the array to be sorted.
 */
void sort_sub_array(char** sub_array, int start, int end);

/// Function that obtains the distance printed in each string as an integer.
int array_get_distance(char* string);
#endif
