/**
 * @brief Main method to calculate the levenshtein distance between two strings in pure ASCII format only. Original source code taken from: https://es.wikipedia.org/wiki/Distancia_de_Levenshtein.
 * Parallel algorithm written by Longjiang Guo1 et al.
 * @param string_1 The first string to be compared.
 * @param string_2 The second string to be compared.
 * @param requested_workers Amount of workers requested in arguments by the user, or all CPU cores otherwise.
 * @return The distance between the two strings calculated by Levenshtein's parallelized algorithm.
 */
size_t levenshtein_distance_ascii(const char* string_1, const char* string_2, size_t requested_workers);

/**
 * @brief Main method to calculate the levenshtein distance between two strings in Unicode format.
 * @param string_1 The first string to be compared.
 * @param string_2 The second string to be compared.
 * @return The distance between the two strings calculated by levenshtein's algorithm.
 */
size_t levenshtein_distance_unicode(const wchar_t* string_1, const wchar_t* string_2, size_t requested_workers);

void levenshtein_init_matrix(int**m, unsigned long long rows, unsigned long long columns);

/// Helper function to fill matrix X needed for Levenshtein's algorithm parallelization. Thread safe.
void* levenshtein_init_matrix_x_ascii(void* data);

void* levenshtein_init_matrix_x_unicode(void* data);

/// Workers function to calculate Levenshtein's distance concurrently.
void* levenshtein_parallel_distance_ascii(void* data);

void* levenshtein_parallel_distance_unicode(void* data);

/// Helper function to initialize the ASCII alphabet, used in Levenshtein's algorithm parallelization.
void levenshtein_init_ascii_alphabet(unsigned char* alphabet);

void levenshtein_init_unicode_alphabet(wchar_t* alphabet);
