# Profiling Levenshtein's implementation

To profile and optimize this program, the tool used was *callgrind* from *valgrind* development tool suite. We ran the program with a middle-size test case (*desiderata*) to figure out which subroutines and specifically which lines of code spend the most CPU clock cycles. From figures 1 and 2 we noticed that the 99.97% of CPU resources needed by our program are consumed by the repeatedly calls to `levenshtein_distance()` subroutine, as it is expected since it's the main process of the program. *Note: drag the mouse over figures to deploy the figure's name.*

![figure_1](figure_1.png "Figure 1")

Therefore, by having a better representation of the function calls tree, as in figure 2, we also noticed that the subroutine called `min()` (a method that returns the minimum between two values) is also an important CPU cycles consumer within the subroutine `levenshtein_distance()`. We assume that the CPU consumption of this subroutine is due to it is in a nested statement, to obtain the minimum between three values, and it is also nested in the inner cycle of the main subroutine that calculates the distance.

![figure_2](figure_2.png "Figure 2")

Then, by making some improvements to the code lines indicated by the profiling tool, the execution time of the program with "desiderata" test case get reduced from 9 to 6 seconds (in a particular low-performance computer) just by changing a double-nested function call to `min()` with a nested ternary operator ( ? : ) to get the minimum value between three distances.

To verify the results thrown by *callgrind* regarding CPU consumption, we also ran the program with profiling features enabled by compilating it with the command `-pg` passed to gcc compiler. After the run with all the test cases recursively, the program generated a file which then we opened using the tool *gprof*, to transform the profiling data in a human-readable form through a text file. And as we expected, the results confirmed that the function `levenshtein_distance()` actually spends the most of the time in our program, as we shall see in figures 3 and 4.

![figure_3](figure_3.png "Figure 3")

Despite there are a lot of function calls in the program, the most important ones (the most time consuming) are the ones shown at the top of figure 4, in which we see that the whole time elapsed was spent almost entirely by the main function `levdist_run()`, that calls to `levdist_process_dirs()` inside it, and this last one calls `levenshtein_distance()` many thousands times; hence the time spent is due to the huge amount of comparisons the program has to compute for a substantial amount of files. Note that the sorting procedures don't consume an important amount of time, since they use the quicksort algorithm to do their work; in fact it is a really quick sorting algorithm.

![figure_4](figure_4.png "Figure 4")

With respect to the cache performance, the analysis made by *cachegrind* asserts that the average cache misses rate is very low, with a maximum rate of 2.6% when writing data in cache L1, as it is shown in figure 5. Nevertheless, the cache misses rate when reading is only of 0.4%; a fact that tells us our program makes a good use of cache memory. A major part of read cache misses is due to the Levenshtein's algorithm itself: to compute the value of a cell, then it has to fetch the values of the cell above, the cell left and the cell in diagonal to it. Thus, if the strings being compared are large, the matrix rows are large too, and since the memory allocates data in a single huge row, each cache line could store only a small part of each row in our program. Fetching the values in the row above the current could cause a cache miss, since the two entire rows being used at a given moment may not fit in cache memory, causing the CPU to stall while the needed data is being retrieved from main memory. Hence the number of cache misses when reading data in Levenshtein's algorithm. The same holds for writing cache misses but in a greater proportion, since each cell computed causes an entire cache line to be marked as dirty, and then to write back the dirty line to main memory.

![figure_5](figure_5.png "Figure 5")

In order to make the program faster, we implemented a parallel version of Levenshtein's algorithm, taken from https://ieeexplore.ieee.org/document/6665373. We ran the algorithm mechanically by comparing two strings in paper with both original and parallel versions. Figures 5 and 6 show the procedure of both algorithms by hand. Since these images were scanned, the reduction process to make them lighter reduced considerably the quality.

![figure_6](rastreo_serial.jpg "Figure 6")

![figure_7](rastreo_parallel.jpg "Figure 7")