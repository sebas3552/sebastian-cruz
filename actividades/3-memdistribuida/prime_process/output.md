En ambas versiones se ejecutó el programa 5 veces, pasando como entrada el rango 1 - 1,000,000 para búsqueda de primos.
En la primer figura se aprecia la ejecución con 2 procesos y 5 threads por proceso:

![threads](threads.png "Figura 1")

En la segunda figura, se muestra la salida para las 5 corridas del programa con 10 procesos, y 1 thread por proceso:

![processes](processes.png "Figura 2")

Vemos que para la versión con threads, el menor tiempo de ejecución fue 6.445359945s, mientras que para la versión con procesos fue 0.978513479s. Desde luego, la implementación mediante memoria distribuída tuvo un mejor rendimiento.
