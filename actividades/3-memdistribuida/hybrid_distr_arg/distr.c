#include <assert.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int start(int, int, int, int);
int end(int, int, int, int);

int main(int argc, char* argv[])
{
	assert(argc >= 2);
	MPI_Init(&argc, &argv);
	
	int my_rank = -1;
	int comm_sz = -1;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);
	
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	int interval_len = b - a;
	
	int my_start = start(a, my_rank, interval_len / comm_sz, interval_len % comm_sz);
	int my_end = end(a, my_rank, interval_len / comm_sz, interval_len % comm_sz);
	
	printf("%s:%d: range [%d, %d[ size %d\n", hostname, my_rank, my_start, my_end, my_end - my_start);
	
	size_t thread_count = 3;
	#pragma omp parallel num_threads(thread_count) default(none) shared(my_start, my_end, len_hostname, interval_len, thread_count, hostname, my_rank, comm_sz)
	{
		int myt_rank = omp_get_thread_num();
		int myt_interval_len = my_end - my_start; //B - A
		int myt_start = start(my_start, myt_rank, myt_interval_len / thread_count, myt_interval_len % thread_count);
		int myt_end = end(my_start, myt_rank, myt_interval_len / thread_count, myt_interval_len % thread_count);
		
		printf("\t%s:%d.%d: range [%d, %d[ size %d\n", hostname, my_rank, myt_rank, myt_start, myt_end, myt_end - myt_start);
	}
	
	MPI_Finalize();
	return 0;
}

int start(int a, int rank, int c, int r)
{
	return a + rank*c + (rank < r ? rank : r);
}

int end(int a, int rank, int c, int r)
{
	return a + (rank+1)*c + ((rank+1) < r ? (rank+1) : r);
}
