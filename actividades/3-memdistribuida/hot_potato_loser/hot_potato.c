
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if(argc == 1){
		fprintf(stderr, "Error! Missing potato!\n");
		exit(1);
	}
	int potato = atoi(argv[1]);
	int comm_sz = -1;
	int my_rank = -1;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	if(my_rank == 0){
		--potato;
		MPI_Send(&potato, 1, MPI_INT, (my_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
	}
	do{
		MPI_Recv(&potato, 1, MPI_INT, (my_rank == 0? comm_sz - 1 : my_rank - 1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		--potato;
		//if potato becomes zero, exploded!
		if(potato == 0){
			printf("Process %d lost the game!\n", my_rank);
			--potato;
			//decrement again so that next processes receive a negative potato and exit the game
		}
		MPI_Send(&potato, 1, MPI_INT, (my_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
	}while(potato > 0);
	printf("Process %d is exiting\n", my_rank);
	MPI_Finalize();
	return 0;
}
