#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define TRUE 1
#define FALSE 0

int start(int, int, int, int);
int end(int, int, int, int);
void get_input(int, int, int*, int*, size_t*);
int is_prime(size_t);

int main(int argc, char* argv[])
{
	
	MPI_Init(&argc, &argv);
	
	int my_rank = -1;
	int comm_sz = -1;
	size_t thread_count = sysconf(_SC_NPROCESSORS_ONLN);
	char hostname[MPI_MAX_PROCESSOR_NAME];
	
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);
	
	int a = 0;
	int b = 0;
	if(argc > 1){
		a = atoi(argv[1]);
		b = atoi(argv[2]);
		if(argc == 4)
			thread_count = atoi(argv[3]);
	}else{
		get_input(my_rank, comm_sz, &a, &b, &thread_count);
	}
	int interval_len = b - a;
	
	int my_start = start(a, my_rank, interval_len / comm_sz, interval_len % comm_sz);
	int my_end = end(a, my_rank, interval_len / comm_sz, interval_len % comm_sz);
	size_t local_primes_count = 0;

	size_t total_threads = 0;
	size_t total_primes_count = 0;
	double start_time = MPI_Wtime();
	#pragma omp parallel num_threads(thread_count) default(none) shared(my_start, my_end, len_hostname, interval_len, thread_count, hostname, my_rank, comm_sz, local_primes_count)
	{
		int myt_rank = omp_get_thread_num();
		int myt_interval_len = my_end - my_start; //B - A
		int myt_start = start(my_start, myt_rank, myt_interval_len / thread_count, myt_interval_len % thread_count);
		int myt_end = end(my_start, myt_rank, myt_interval_len / thread_count, myt_interval_len % thread_count);
		for(size_t st = myt_start; st < (size_t)myt_end; ++st){
			#pragma omp critical
			local_primes_count += is_prime(st);
		}
	}
	if(my_rank != 0){
		MPI_Send(&thread_count, 1, MPI_UNSIGNED_LONG, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&local_primes_count, 1, MPI_UNSIGNED_LONG, 0, 0, MPI_COMM_WORLD);
	}else{
		int received_count = 0;
		total_threads += thread_count;
		total_primes_count += local_primes_count;
		while(received_count < comm_sz-1){
			MPI_Recv(&thread_count, 1, MPI_UNSIGNED_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&local_primes_count, 1, MPI_UNSIGNED_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			total_threads += thread_count;
			total_primes_count += local_primes_count;
			++received_count;
		}
		double end_time = MPI_Wtime();
		printf("%zu primes found in range [%d, %d[  in %.9lfs with %d processes and %zu threads\n", total_primes_count , a, b, (end_time - start_time), comm_sz, total_threads);
	}
	MPI_Finalize();
	return 0;
}

int start(int a, int rank, int c, int r)
{
	return a + rank*c + (rank < r ? rank : r);
}

int end(int a, int rank, int c, int r)
{
	return a + (rank+1)*c + ((rank+1) < r ? (rank+1) : r);
}

void get_input(int rank, int comm_sz, int* a, int* b, size_t* thread_count)
{
	if(rank == 0){
		puts("Enter a, b and thread count:");
		scanf("%d%d%zu", a, b, thread_count);
		for(int dest = 1; dest < comm_sz; ++dest){
			MPI_Send(a, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
			MPI_Send(b, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
			MPI_Send(thread_count, 1, MPI_UNSIGNED_LONG, dest, 0, MPI_COMM_WORLD);
		}
	}else{
		MPI_Recv(a, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(b, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(thread_count, 1, MPI_UNSIGNED_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
}

int is_prime(size_t number)
{
	if ( number < 2 )
		return FALSE;
		
	if ( number == 2 )
		return TRUE;
		
	if ( number % 2 == 0 ) 
		return FALSE;

	for ( size_t i = 3, last = (size_t)(double)sqrt(number); i <= last; i += 2 )
		if ( number % i == 0 )
			return FALSE;

	return TRUE;
}
