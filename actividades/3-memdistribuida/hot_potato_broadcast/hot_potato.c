#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define DISPL 4
#define POTATO 0
#define PLAYERS 1
#define CURRENT_PLAYER 2
#define NEXT_PLAYER 3
#define FALSE 0
#define TRUE 1

int collatz_number(int);
int next_player(int, int*, int);

int main(int argc, char* argv[])
{
	if(argc < 2){
		fprintf(stderr, "Error! Missing arguments!\n");
		exit(1);
	}
	const int potato = atoi(argv[1]);
	const int first_player = (argc > 2 ? atoi(argv[2]) : 0);
	assert(potato > 1);
	int comm_sz = -1;
	int my_rank = -1;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	assert(comm_sz > 1);
	assert(first_player <= comm_sz);
	//buffer for the potato and the player count
	const int DATA_SIZE = comm_sz + DISPL;
	const int my_index = my_rank + DISPL;
	int data[DATA_SIZE];
	data[POTATO] = potato;
	data[PLAYERS] = comm_sz;
	for(int index = DISPL; index < DATA_SIZE; ++index)
		data[index] = TRUE;	//all the processes are allowed to play
	data[CURRENT_PLAYER] = first_player;
	//First process starts the game
	if(my_rank == first_player){
		data[POTATO] = collatz_number(data[POTATO]);
		data[NEXT_PLAYER] = next_player(my_rank, data, DATA_SIZE);
		//if the potato exploded to the first process, it exits the game
		if(data[POTATO] == 1){
			--data[PLAYERS];
			data[my_index] = FALSE;
		}
		//first process broadcasts the data array to all other processes
		MPI_Bcast(data, DATA_SIZE, MPI_INT, data[CURRENT_PLAYER], MPI_COMM_WORLD);
	}else{
		//other processes receive the data array from current player, that is, the first player in the game
		MPI_Bcast(data, DATA_SIZE, MPI_INT, data[CURRENT_PLAYER], MPI_COMM_WORLD);
	}
	do{
		if(data[NEXT_PLAYER] == my_rank){ //if it's my turn
			data[CURRENT_PLAYER] = data[NEXT_PLAYER]; //i'm the current now
			data[NEXT_PLAYER] = next_player(my_rank, data, DATA_SIZE);
			if(data[PLAYERS] == 1){
				printf("Process %d won the game!\n", my_rank);
				data[my_index] = FALSE;
				--data[PLAYERS];
				data[POTATO] = -1;
			}else{
				data[POTATO] = collatz_number(data[POTATO]);
				printf("Potato: %d\n", data[POTATO]);
				//if the potato becomes one, it explodes and current process loses
				if(data[POTATO] == 1){
					data[POTATO] = potato; //reset the potato for next player, current player can't continue playing
					--data[PLAYERS];	   //decrement the player count
					data[my_index] = FALSE;
					printf("Process %d has lost\n", my_rank);
				}
			}
			MPI_Bcast(data, DATA_SIZE, MPI_INT, data[CURRENT_PLAYER], MPI_COMM_WORLD);	
		}else{
			MPI_Bcast(data, DATA_SIZE, MPI_INT, data[NEXT_PLAYER], MPI_COMM_WORLD);
		}
	}while(data[PLAYERS] > 0);
	MPI_Finalize();
	return 0;
}

int collatz_number(int n)
{
	if(n == 1)
		return n;
	if(n % 2 == 0)
		return n / 2;
	else
		return 3 * n + 1;
}

int next_player(int current_player, int* data, int data_size)
{
	int current_pos = current_player + DISPL;
	for(int next = (current_pos + 1) % data_size; next != current_pos; next = (next + 1) % data_size){
		if(next < DISPL)
			continue;
		if(data[next])
			return next - DISPL;
	}
	return current_player;
}
