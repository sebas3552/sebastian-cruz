#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define DATA_SIZE 2
#define POTATO 0
#define PLAYERS 1
#define FALSE 0
#define TRUE 1

int main(int argc, char* argv[])
{
	if(argc < 2){
		fprintf(stderr, "Error! Missing arguments!\n");
		exit(1);
	}
	const int potato = atoi(argv[1]);
	const int first_player = atoi(argv[2]);
	assert(potato > 1);
	int comm_sz = -1;
	int my_rank = -1;
	int won = FALSE;
	int lost = FALSE;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	assert(comm_sz > 1);
	assert(first_player <= comm_sz);
	//buffer for the potato and the player count
	int data[DATA_SIZE];
	data[POTATO] = potato;
	data[PLAYERS] = comm_sz;
	
	//Process zero starts the game
	if(my_rank == first_player){
		//--data[POTATO];
		if(data[POTATO] % 2 == 0)
			data[POTATO] /= 2;
		else
			data[POTATO] = 3 * data[POTATO] + 1;
		MPI_Send(data, DATA_SIZE, MPI_INT, (my_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
	}
	do{
		MPI_Recv(data, DATA_SIZE, MPI_INT, (my_rank == 0? comm_sz - 1 : my_rank - 1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if(!lost && data[PLAYERS] == 1){
			printf("Process %d won the game!\n", my_rank);
			won = TRUE;
			--data[PLAYERS];	//if current process wins, sets the potato to 0 to let other processes know the game is over
		}
		//if current process is playing, that is, hasn't lost neither won, calculate the next collatz number for the potato and pass it around
		if(!lost && !won && data[PLAYERS] > 0){
			if(data[POTATO] % 2 == 0)
				data[POTATO] /= 2;
			else
				data[POTATO] = 3 * data[POTATO] + 1;
			//if the potato becomes one, it explodes and current process loses
			if(data[POTATO] == 1){
				data[POTATO] = potato; //reset the potato for next player, current player can't continue playing
				--data[PLAYERS];	   //decrement the player count
				lost = TRUE;
				printf("Process %d has lost\n", my_rank);
			}
		}
		MPI_Send(data, DATA_SIZE, MPI_INT, (my_rank + 1) % comm_sz, 0, MPI_COMM_WORLD);
	}while(data[PLAYERS] > 0);
	MPI_Finalize();
	return 0;
}
