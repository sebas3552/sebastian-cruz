En las tres versiones de esta actividad se ejecutó el programa cinco veces, pasando como entrada el rango 1 - 10,000,000 para búsqueda de primos.
En la primer figura se aprecia la ejecución con el mejor tiempo para la actividad *prime_hybrid_int*, con 10 procesos y 10 threads por proceso:

![threads](prime_hybrid_int.png "Figura 1")

En la segunda figura, se muestra la salida de la ejecución con el mejor tiempo para la actividad *prime_process*, con 10 procesos, y 1 thread por proceso:

![processes](prime_process.png "Figura 2")

Finalmente, en la figura siguiente se muestra la salida de la ejecución con el mejor tiempo para la actividad *prime_process_reduction*, con 10 procesos y 1 thread por proceso:

![processes_reduction](prime_process_reduction.png "Figura 3")

Se aprecia que el mejor tiempo lo tuvo el programa de la actividad 43 *prime_process*, con aproximadamente 0.975s, aunque no fue muy distinto del tiempo obtenido para la versión implementada mediante reducción, la cual registró su mejor tiempo con 0.977s. La versión híbrida de la actividad 42 *prime_hybrid_int* tuvo 1.213s como su mejor marca.

Cabe destacar que se hicieron las ejecuciones de cada versión del programa con apenas 10 procesos en el clúster arenal, pues al crear grandes cantidades de procesos se mostraba un error desconocido, propio de la creación de los procesos por *mpiexec*.
