#include <mpi.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);

	int my_rank = -1;
	int process_count = -1;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);

	char hostname[MPI_MAX_PROCESSOR_NAME];
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);

	printf("Hello from process %d of %d on %s\n", my_rank, process_count, hostname);
	
	size_t thread_count = (size_t)sysconf(_SC_NPROCESSORS_ONLN);
	#pragma omp parallel num_threads(thread_count) default(none) shared(len_hostname, my_rank, process_count, hostname, thread_count)
	{
		size_t thread_number = omp_get_thread_num();
		printf("\tHello from thread %zu of %zu of process %d on %s\n", thread_number, thread_count, my_rank, hostname);
	}

	MPI_Finalize();
	return 0;
}
