#include "goldbachcalculator.h"
#include "goldbachworker.h"
#include <iostream>
#include <fstream>
#include <QTimer>

GoldbachCalculator::GoldbachCalculator(QObject *parent)
    : QAbstractListModel(parent), totalSums(0)
{
}

void GoldbachCalculator::calculate(long long number)
{
    this->beginResetModel();
    int ideal = QThread::idealThreadCount() - 1;
    this->results.resize(ideal);
    this->workerCount = 0;
    for( int current = 0; current < ideal; ++current, ++workerCount)
    {
        this->workers.append(new GoldbachWorker(number, current, ideal, this->results[current], this));
        this->connect( workers[current], &GoldbachWorker::calculationDone, this, &GoldbachCalculator::workerDone );
        this->connect( workers[current], &GoldbachWorker::updatePercent, this, &GoldbachCalculator::calculatePercent );
        this->workers[current]->start();
    }
    this->fetchedRowCount = 0;
    this->endResetModel();
}

void GoldbachCalculator::stop()
{
    for(int i = 0; i < workers.size(); ++i){
        if(workers[i])
            this->workers[i]->requestInterruption();
    }
    emit calculationDone(totalSums);
}

long long GoldbachCalculator::totalResults() const
{
    long long total = 0;
    for(int i = 0; i < this->results.size(); ++i){
        total += this->results[i].size();
    }
    return total;
}

QVector<QString> GoldbachCalculator::getAllSums()
{
    QVector<QString> sums;
    for(int i = 0; i < this->results.size(); ++i){
        for(int j = 0; j < this->results[i].size(); ++j){
            if(!this->results[i].empty())
                sums.append(this->results[i][j]);
        }
    }
    return sums;
}

int GoldbachCalculator::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->fetchedRowCount;
}

QVariant GoldbachCalculator::data(const QModelIndex &index, int role) const
{
    if ( ! index.isValid() )
        return QVariant();

    if ( index.row() < 0 || index.row() >= this->totalResults() )
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        int currentRow = 0, accumulate = 0;
        while(index.row() >= this->results[currentRow].size()+accumulate){
            accumulate += this->results[currentRow].size();
            ++currentRow;
        }

        QString result = this->results[currentRow][index.row() - accumulate];
        return result;
    }

    return QVariant();
}

bool GoldbachCalculator::canFetchMore(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->fetchedRowCount < this->totalResults();
}

void GoldbachCalculator::fetchMore(const QModelIndex &parent)
{
	Q_UNUSED(parent);
    int remainder = this->totalResults() - this->fetchedRowCount;
    int itemsToFetch = qMin(100, remainder);

    if (itemsToFetch <= 0)
        return;

    int firstRow = this->fetchedRowCount;
    int lastRow = this->fetchedRowCount + itemsToFetch;

    beginInsertRows(QModelIndex(), firstRow, lastRow);
    this->fetchedRowCount += itemsToFetch;
    endInsertRows();
}

void GoldbachCalculator::workerDone(long long sumCount, int workerNumber)
{
    totalSums += sumCount;
    if(!this->workers[workerNumber]->isRunning())
        QTimer::singleShot(5, this->workers[workerNumber], SLOT(deleteLater()));
    this->fetchMore(QModelIndex());
    this->workers[workerNumber] = nullptr;
    --this->workerCount;
    if(this->workerCount < 1){
        int number = 0;
        QVector<QString> temp;
        QString str;
        for(int i = 0; i < this->results.size(); ++i){
            for(int j = 0; j < this->results[i].size(); ++j){
                if(!this->results[i].empty() && this->results[i][j] != nullptr){
                   str = this->results[i][j];
                   str.prepend(tr("%1: ").arg(++number));
                   temp.append(str);
                }
            }
            if(!this->results[i].empty()){
                this->results.replace(i, temp);
                temp.clear();
            }
        }
        emit this->calculationDone(totalSums);
    }
}

void GoldbachCalculator::calculatePercent()
{
    int total = 0;
    for(int i = 0; i < workers.size(); ++i){
        if(workers[i]){
            total += workers[i]->myPercent;        
        }
    }
    currentPercent = (total/workerCount >= currentPercent && total/workerCount < 100? total/workerCount : currentPercent);
    emit updatePercent(currentPercent);
}
