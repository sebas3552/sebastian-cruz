#include <iostream>
#include <QDir>
#include <QFile>
#include <QTextStream>

#include "goldbachcalculator.h"
#include "goldbachtester.h"

GoldbachTester::GoldbachTester(int &argc, char **argv)
    : QCoreApplication(argc, argv)
{
}

int GoldbachTester::run()
{
    bool canTest = true;
    for ( int index = 1; index < this->arguments().count(); ++index )
        canTest = this->testDirectory( this->arguments()[index] );
    if(canTest)
        return this->exec();
    else
        return 1;
}

bool GoldbachTester::testDirectory(const QString &path)
{
    std::cout << "Directory : " << qPrintable(path) << std::endl;

    QDir dir(path);
    if ( ! dir.exists() )
        return std::cerr << "error: could not open directory: " << qPrintable(path) << std::endl, false;

    dir.setFilter(QDir::Files);

    QFileInfoList fileList = dir.entryInfoList();
    this->totalCases = fileList.count();
    for ( int index = 0; index < fileList.count(); ++index )
        this->testFile( fileList[index] );
    return true;
}

bool GoldbachTester::testFile(const QFileInfo &fileInfo)
{
    std::cout << "Testing " << qPrintable( fileInfo.filePath() ) << std::endl;

    bool ok = true;
    long long number = fileInfo.baseName().toLongLong(&ok);
    if ( ! ok )
        return std::cerr << "error: invalid number: " << qPrintable(fileInfo.fileName()) << std::endl, false;

    GoldbachCalculator* goldbachCalculator = new GoldbachCalculator(this);
    this->calculators.insert(goldbachCalculator, fileInfo);
    this->connect( goldbachCalculator, &GoldbachCalculator::calculationDone, this, &GoldbachTester::calculationDone );
    goldbachCalculator->calculate(number);

    return true;
}

void GoldbachTester::calculationDone(long long sumCount)
{
    Q_UNUSED(sumCount);
    GoldbachCalculator* goldbachCalculator = dynamic_cast<GoldbachCalculator*>( sender() );
    Q_ASSERT(goldbachCalculator);
    const QFileInfo& fileInfo = this->calculators.value( goldbachCalculator );
    const QVector<QString>& expectedSums = this->loadLines(fileInfo);
    const QVector<QString>& calculatorSums = goldbachCalculator->getAllSums();
    bool failed = false;
    for(int i = 0; i < calculatorSums.size() && i < expectedSums.size(); ++i){
        if(expectedSums.at(i) != calculatorSums.at(i)){
            std::cerr << "test case " << qPrintable(fileInfo.fileName()) << " failed at line " << i+1 << ": " << std::endl;
            std::cerr << "expected: " << qPrintable(expectedSums.at(i)) << std::endl;
            std::cerr << "produced: " << qPrintable(calculatorSums.at(i)) << std::endl;
            ++this->failed;
            failed = true;
            break;
        }
    }
    if(!failed)
        std::cout << "test case passed: " << qPrintable(fileInfo.fileName()) << std::endl;
    goldbachCalculator->deleteLater();
    this->calculators.remove( goldbachCalculator );
    if ( this->calculators.count() <= 0 ){
        this->report();
        this->quit();
    }
}

QVector<QString> GoldbachTester::loadLines(const QFileInfo &fileInfo)
{
    QVector<QString> result;

    QFile file( fileInfo.absoluteFilePath() );
    if ( ! file.open(QFile::ReadOnly) )
        return std::cerr << "error: could not open: " << qPrintable(fileInfo.fileName()) << std::endl, result;

    QTextStream textStream( &file );
    while ( ! textStream.atEnd() )
        result.append( textStream.readLine() );

    return result;
}

void GoldbachTester::report()
{
    this->passed = this->totalCases-this->failed;
    int passedPercentage = 100*passed/totalCases;
    std::cout << totalCases << " test cases: " << passed << " passed (" << passedPercentage << "\%), " << failed << " failed (" << 100-passedPercentage << "\%)." << std::endl;
}
