#ifndef GOLDBACHCALCULATOR_H
#define GOLDBACHCALCULATOR_H

#include <QAbstractListModel>
#include <QVector>

class GoldbachWorker;

class GoldbachCalculator : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(GoldbachCalculator)

  protected:
    int fetchedRowCount = 0;
    long long totalSums = 0;
    int currentPercent = 0;
    int workerCount = 0;
    QVector<GoldbachWorker *> workers;
    QVector<QVector<QString>> results;

  public:
    explicit GoldbachCalculator(QObject *parent = nullptr);
    void calculate(long long number);
    void stop();
    QVector<QString> getAllSums();

    // Overriden methods from QAbastractListModel
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  protected:
    virtual bool canFetchMore(const QModelIndex &parent) const override;
    virtual void fetchMore(const QModelIndex &parent) override;
    long long totalResults() const;

  signals:
    void calculationDone(long long sumCount);
    void updatePercent(int percent);

  protected slots:
    void workerDone(long long sumCount, int workerNumber);
    void calculatePercent();
};

#endif // GOLDBACHCALCULATOR_H
