#include <QtMath>
#include <QTime>
#include <iostream>
#include "goldbachworker.h"


GoldbachWorker::GoldbachWorker(long long number, int workerNumber, int workerCount, QVector<QString>& results, QObject *parent)
    : QThread(parent), number(number), workerNumber(workerNumber), workerCount(workerCount), myPercent(0), results(results)
{
    timer = new QTime();
}

GoldbachWorker::~GoldbachWorker()
{
    delete timer;
}

void GoldbachWorker::run()
{
    long long sumCount = this->calculate(number);   
    emit this->calculationDone(sumCount, workerNumber);   
}

long long GoldbachWorker::calculate(long long number)
{

    long partialSums = ceil((double)number/(double)workerCount);
    long long start = 0;
    long long end = partialSums-1;
    for(int i = 0; i < workerNumber; ++i){
        start += partialSums;
        end += partialSums;
        if(i == workerCount-1 && number % 2 == 0)
            ++end;
    }
    start = (start == 0? 2 : start);
    end = (end > number? number : end);
    /*Mejora
    *Si el intervalo que le corresponde está entre number/2 y number (de la mitad para arriba)
    *retorna 0 inmediatamente sin buscar sumas, pues no va a encontrar ninguna.
    */
    if(start > number/2){
#ifndef TESTING
        std::cout << "Worker " << workerNumber << " found " << 0 << " sums from " << start << " to " << end << " in " << 0 << " seconds." << std::endl;
#endif
        return 0;
    }
    timer->start();
    if ( number < 4 || number == 5 ) return 0;
    long long sums = number % 2 == 0 ? calculateEvenGoldbach(number, start, end) : calculateOddGoldbach(number, start, end);
    #ifndef TESTING
        double totalTime = timer->elapsed() / 1000.0;
        std::cout << "Worker " << workerNumber << " found " << sums << " sums from " << start << " to " << end << " in " << totalTime << " seconds." << std::endl;
    #endif
    return sums;
}

long long GoldbachWorker::calculateEvenGoldbach(long long number, long long start, long long end)
{
    long long results = 0;
    int vals = end - start;
    /*Indica el valor de cada número verificado en términos de porcentaje con respecto al número total.*/
    double advance = (1.0/(double)vals)*100;
    /*Contador al que se le va sumando advance por cada ciclo hasta que supere el 1%.*/
    double count = 0;
    for ( long long a = start; a < number && a <= end; ++a, count += advance )
    {
        /*Mejora
        *Envía el aumento de la barra de progreso sii representa 1% o más.
        */
        if(count >= 1){
            ++myPercent;
            count = 0;
            emit updatePercent();
        }
        /*Mejora
        *Llama a isPrime() solo si el número es impar, reduciendo a la mitad las invocaciones a isPrime().
        * Calcula la paridad del número con un and a nivel de bits en vez de dividir entre 2.
        */
        if ( (a != 2) && (!(a & 1) ||! isPrime(a) )) continue;
        long long b = number - a;
        if ( b >= a && isPrime(b) ){
            this->results.append( tr("%1 + %2").arg(a).arg(b) );
            ++results;
        }
        /*Si el usuario canceló, detener los cálculos.*/
        if ( this->isInterruptionRequested() )
            return results;
    }
    return results;
}

long long GoldbachWorker::calculateOddGoldbach(long long number, long long start, long long end)
{
    /*Mismas mejoras de calculateEvenGoldbach.*/
    long long results = 0;
    int vals = end - start;
    double advance = (1.0/(double)vals)*100;
    double count = 0;
    for ( long long a = start; a < number && a <= end; ++a, count += advance)
    {
        if(count >= 1){
            ++myPercent;
            count = 0;
            emit updatePercent();
        }
        if ( (a != 2) && (!(a & 1) ||! isPrime(a) )) continue;
        for ( long long b = a; b < number; ++b )
        {
            if ((b != 2) && (!(b & 1) || ! isPrime(b)) ) continue;
            long long c = number - a - b;
            if ( c >= b && isPrime(c) ){
                this->results.append( tr("%1 + %2 + %3").arg(a).arg(b).arg(c) );
                ++results;
            }

            /*Si el usuario canceló, detener los cálculos.*/
            if ( this->isInterruptionRequested() )
                return results;
        }
    }
    return results;
}

bool GoldbachWorker::isPrime(long long number)
{

    if (number < 2) return false;
    if(number == 2) return true;
    /*Mejora
    * Calcula si el número es par a nivel de bits, y devuelve false si lo es.*
    */
    if(!(number & 1)) return false;
    /*Mejora
    *Avanza de 2 en 2 en el ciclo, revisa solo los números impares.
    */
    for ( long long i = 3, last = qSqrt(number); i <= last; i += 2 )
        if (number % i == 0)
            return false;
    return true;
}
