#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
void* hello(void* data)
{
	size_t thread_number =  (size_t)data;
	printf("Hello from thread %zu\n", thread_number);
	return 0;
}

int main(int argc, char* argv[])
{
	size_t threads_count = 0;
	if(argv[1])
		threads_count = (size_t) atoi(argv[1]);
	else
		threads_count = sysconf(_SC_NPROCESSORS_ONLN);
	pthread_t* threads = (pthread_t*)calloc(threads_count, sizeof(pthread_t));
	for(size_t index = 0; index < threads_count; ++index){
		pthread_create(&threads[index], NULL, hello, (void*) (index+1));
	}
	printf("Hello from main thread\n");
	for(size_t index = threads_count-1; index <= 0; --index){
		pthread_join(threads[index], NULL); //wait for thread until it finishes it's execution.
	}
	free(threads);
	return 0;
}
