#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

#include "concurrency.h"



typedef struct thread_data{
	size_t thread_number;
	size_t total_threads;
	sem_t* semaphores;
	pthread_t thread;
} thread_data_t;

void* hello(void* data)
{
	thread_data_t* my_data =  (thread_data_t*) data;
	
	sem_wait(&(my_data->semaphores[my_data->thread_number]));
	printf("Hello from thread %zu of %zu\n", my_data->thread_number, my_data->total_threads);
	sem_post(&(my_data->semaphores[(my_data->thread_number + 1)%my_data->total_threads]));	
	return 0;
	
}

int main(int argc, char* argv[])
{
	const size_t MAX_THREADS = 32751;
	(void) argc; //unused variable
	const int SEM_RED = 0;
	size_t thread_count = 0;
	if(argv[1])
		thread_count = ((size_t) atoi(argv[1]) < MAX_THREADS? (size_t) atoi(argv[1]) : MAX_THREADS);
	else
		thread_count = concurrency_cpu_count();
	walltime_t time;
	walltime_start(&time);
	sem_t* global_semaphores = (sem_t*)calloc(thread_count, sizeof(sem_t)); //semaphores array
	
	for(int i = 0; i < (int)thread_count; ++i){
		sem_init(&global_semaphores[i], 0, SEM_RED);  //initializes semaphores to red
	}
	sem_post(&global_semaphores[0]); //sets to green the first semaphore
	thread_data_t* thread_structs = (thread_data_t*)calloc(thread_count, sizeof(thread_data_t));
	for(size_t index = 0; index < thread_count; ++index){
		thread_structs[index].thread_number = (size_t)index;
		thread_structs[index].total_threads = thread_count;
		thread_structs[index].semaphores = global_semaphores;
		pthread_create((&thread_structs[index].thread), NULL, hello, (void*) (&thread_structs[index]));
	}
	printf("Hello from main thread\n");
	for(size_t index = 0; index < thread_count; ++index){
		pthread_join(thread_structs[index].thread, NULL); //wait for thread until it finishes it's execution.
		sem_destroy(&global_semaphores[index]);
	}
	free(thread_structs);
	free(global_semaphores);
	double seconds_elapsed = walltime_elapsed(&time);
	printf("Elapsed %lfs with %d threads\n", seconds_elapsed, (int)thread_count);
	return 0;
}
