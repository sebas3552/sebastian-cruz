#include <assert.h>
#include <stdlib.h>

#include "array_mutex.h"

array_mutex_t* array_mutex_create(size_t capacity)
{
	assert(capacity);
	array_mutex_t* new_array = (array_mutex_t*)calloc(1, sizeof(array_mutex_t));
	new_array->elements = (void**)calloc(capacity, sizeof(void*));
	new_array->capacity = capacity;
	new_array->count = 0;
	pthread_mutex_init(&new_array->mutex, NULL);
	return new_array;
}

void array_mutex_destroy(array_mutex_t* array)
{
	assert(array);
	free(array->elements);
	pthread_mutex_destroy(&array->mutex);
	free(array);
}

//private function used by append
int array_mutex_increase_capacity(array_mutex_t* array)
{
	size_t new_capacity = 10 * array->capacity;
	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL )
		return -1;
		
	array->capacity = new_capacity;
	return 0; // Success
}

//private function used by remove
int array_mutex_decrease_capacity(array_mutex_t* array)
{
	size_t new_capacity = array->capacity / 10;
	if ( new_capacity < 10 ){
		return 0;
	}

	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL ){
		return -1;
	}

	array->capacity = new_capacity;
	return 0; // Success
}

size_t array_mutex_get_count(array_mutex_t* array)
{
	assert(array);
	return array->count;
}

void* array_mutex_get_element(array_mutex_t* array, size_t index)
{
	pthread_mutex_lock(&array->mutex);
	if( index < array_mutex_get_count(array) ){
		void* returned_element = array->elements[index];
		pthread_mutex_unlock(&array->mutex);
		return returned_element;
	}else{
		pthread_mutex_unlock(&array->mutex);
		return NULL;
	}
}

int array_mutex_append(array_mutex_t* array, void* element)
{
	pthread_mutex_lock(&array->mutex);
	if ( array->count == array->capacity )
		if ( ! array_mutex_increase_capacity(array) ){
			pthread_mutex_unlock(&array->mutex);
			return -1;
		}

	array->elements[array->count++] = element;
	pthread_mutex_unlock(&array->mutex);
	return 0; // Success
}

int array_mutex_find_first(array_mutex_t* array, const void* element, size_t start_pos)
{
	pthread_mutex_lock(&array->mutex);
	for ( size_t index = start_pos; index < array->count; ++index )
		if ( array->elements[index] == element ){
			pthread_mutex_unlock(&array->mutex);
			return (int)index;
		}
	pthread_mutex_unlock(&array->mutex);
	return array_mutex_not_found;
}

int array_mutex_remove_first(array_mutex_t* array, const void* element, size_t start_pos)
{
	int index = array_mutex_find_first(array, element, start_pos);
	if ( index == array_mutex_not_found ){
		return -1;
	}

	pthread_mutex_lock(&array->mutex);
	for ( --array->count; (size_t)index < array->count; ++index )
		array->elements[index] = array->elements[index + 1];
	if ( array->count == array->capacity / 10 )
		array_mutex_decrease_capacity(array);
	pthread_mutex_unlock(&array->mutex);
	return 0; // Removed
}
