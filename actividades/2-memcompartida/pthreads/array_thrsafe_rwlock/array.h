#ifndef ARRAY_H
#define ARRAY_H

#include <pthread.h>
#include <stddef.h>


typedef struct array
{
	void** elements;
	size_t capacity;
	size_t count;
	pthread_rwlock_t rwlock;
} array_t;

static const int array_not_found = -1;

array_t* array_create(size_t capacity);
void array_destroy(array_t* array);
int array_append(array_t* array, void* element);
size_t array_get_count(array_t* array);
void* array_get_element(array_t* array, size_t index);
int array_find_first(array_t* array, const void* element, size_t start_pos);
int array_remove_first(array_t* array, const void* element, size_t start_pos);

#endif // ARRAY_H
