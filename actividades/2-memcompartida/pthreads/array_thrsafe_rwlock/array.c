#include <assert.h>
#include <stdlib.h>

#include "array.h"

array_t* array_create(size_t capacity)
{
	assert(capacity);
	array_t* new_array = (array_t*)calloc(1, sizeof(array_t));
	new_array->elements = (void**)calloc(capacity, sizeof(void*));
	new_array->capacity = capacity;
	new_array->count = 0;
	pthread_rwlock_init(&new_array->rwlock, NULL);
	return new_array;
}

void array_destroy(array_t* array)
{
	assert(array);
	free(array->elements);
	pthread_rwlock_destroy(&array->rwlock);
	free(array);
}

//private function used by append
int array_increase_capacity(array_t* array)
{
	size_t new_capacity = 10 * array->capacity;
	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL ){
		return -1;
	}
		
	array->capacity = new_capacity;
	return 0; // Success
}

//private function used by remove
int array_decrease_capacity(array_t* array)
{
	size_t new_capacity = array->capacity / 10;
	if ( new_capacity < 10 ){
		return 0;
	}

	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL ){
		return -1;
	}

	array->capacity = new_capacity;
	return 0; // Success
}

size_t array_get_count(array_t* array)
{
	pthread_rwlock_rdlock(&array->rwlock);
	assert(array);
	size_t count = array->count;
	pthread_rwlock_unlock(&array->rwlock);
	return count;
}

void* array_get_element(array_t* array, size_t index)
{
	pthread_rwlock_rdlock(&array->rwlock);
	if( index < array_get_count(array) ){
		void* returned_element = array->elements[index];
		pthread_rwlock_unlock(&array->rwlock);
		return returned_element;
	}else{
		pthread_rwlock_unlock(&array->rwlock);
		return NULL;
	}
}

//append writes in the array, then wrlock is needed
int array_append(array_t* array, void* element)
{
	pthread_rwlock_wrlock(&array->rwlock);
	if ( array->count == array->capacity )
		if ( ! array_increase_capacity(array) ){
			pthread_rwlock_unlock(&array->rwlock);
			return -1;
		}

	array->elements[array->count++] = element;
	pthread_rwlock_unlock(&array->rwlock);
	return 0; // Success
}

//find is a read-only function, then rdlock is needed
int array_find_first(array_t* array, const void* element, size_t start_pos)
{
	pthread_rwlock_rdlock(&array->rwlock);
	for ( size_t index = start_pos; index < array->count; ++index ){
		if ( array->elements[index] == element ){
			pthread_rwlock_unlock(&array->rwlock);
			return (int)index;
		}
	}
	pthread_rwlock_unlock(&array->rwlock);
	return array_not_found;
}

//remove modifies the array, then wrlock is needed
int array_remove_first(array_t* array, const void* element, size_t start_pos)
{
	//the lock is in find_first, this part is read-only
	int index = array_find_first(array, element, start_pos);
	if ( index == array_not_found )
		return -1;

	//now the array needs wrlock to modify its elements
	pthread_rwlock_wrlock(&array->rwlock);
	for ( --array->count; (size_t)index < array->count; ++index )
		array->elements[index] = array->elements[index + 1];
	if ( array->count == array->capacity / 10 )
		array_decrease_capacity(array);
	pthread_rwlock_unlock(&array->rwlock);
	return 0; // Removed
}
