#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct thread_data{
	size_t thread_number;
	size_t total_threads;
	pthread_t thread;
} thread_data_t;

void* hello(void* data)
{
	thread_data_t* my_data =  (thread_data_t*) data;
	printf("Hello from thread %zu of %zu\n", my_data->thread_number, my_data->total_threads);
	return 0;
}

int main(int argc, char* argv[])
{
	(void) argc;
	size_t threads_count = 0;
	if(argv[1])
		threads_count = (size_t) atoi(argv[1]);
	else
		threads_count = sysconf(_SC_NPROCESSORS_ONLN);
	thread_data_t* thread_structs = (thread_data_t*)calloc(threads_count, sizeof(thread_data_t));
	for(size_t index = 0; index < threads_count; ++index){
		thread_structs[index].thread_number = (size_t)index+1;
		thread_structs[index].total_threads = threads_count;
		pthread_create((&thread_structs[index].thread), NULL, hello, (void*) (&thread_structs[index]));
	}
	printf("Hello from main thread\n");
	for(size_t index = 0; index < threads_count; ++index){
		pthread_join(thread_structs[index].thread, NULL); //wait for thread until it finishes it's execution.
	}
	free(thread_structs);
	return 0;
}
