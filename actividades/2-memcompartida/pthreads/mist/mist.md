Actividad 23: rastreo del programa `mist.md`

En las figuras 1 y 2 se observa el rastreo del programa, en papel, para identificar su funcionalidad. El rastreo supone que cada instrucción ocupa un ciclo de máquina, que todos los hilos llevan el mismo ritmo sin ser interrumpidos por el sistema operativo, y que particularmente la función `sleep(n)` duerme al hilo n ciclos de máquina. Nota: debido a que las imágenes fueron escaneadas, su transformación para reducir el tamaño afectó considerablemente la definición de las mismas.

![figure_1](rastreo_mist1.jpg "Figure 1")

![figure_2](rastreo_mist2.jpg "Figure 2")

¿Qué trabajo hace la función `mistery()`? Estudiando detenidamente el comportamiento del programa mediante el rastreo, se concluye que esta función implementa lo que en el fondo es una *barrera* para 3 hilos en particular, a través de `mutex` y variables de condición. No obstante, la salida de los hilos después de alcanzar el límite de la barrera no es simultánea, dado que el `mutex` dentro de la función sólo permite salir a un hilo a la vez. Sin embargo, como no existen más instrucciones entre la instrucción que libera todos los hilos (`pthread_cond_broadcast()`) y la que libera el `mutex`, se puede decir que la salida de los hilos después de pasar por la barrera es casi simultánea.