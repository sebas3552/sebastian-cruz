#include <assert.h>
#include <stdlib.h>

#include "array.h"

array_t* array_create(size_t capacity)
{
	assert(capacity);
	array_t* new_array = (array_t*)calloc(1, sizeof(array_t));
	new_array->elements = (void**)calloc(capacity, sizeof(void*));
	new_array->capacity = capacity;
	new_array->count = 0;
	return new_array;
}

void array_destroy(array_t* array)
{
	assert(array);
	free(array->elements);
	free(array);
}

//private function used by append
int array_increase_capacity(array_t* array)
{
	size_t new_capacity = 10 * array->capacity;
	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL )
		return -1;
		
	array->capacity = new_capacity;
	return 0; // Success
}

//private function used by remove
int array_decrease_capacity(array_t* array)
{
	size_t new_capacity = array->capacity / 10;
	if ( new_capacity < 10 )
		return 0;

	array->elements = (void**)realloc( array->elements, new_capacity * sizeof(void*) );
	if ( array->elements == NULL )
		return -1;

	array->capacity = new_capacity;
	return 0; // Success
}

size_t array_get_count(const array_t* array)
{
	assert(array);
	return array->count;
}

void* array_get_element(array_t* array, size_t index)
{
	assert( index < array_get_count(array) );
	void* returned_element = array->elements[index];
	return returned_element;
}

int array_append(array_t* array, void* element)
{
	if ( array->count == array->capacity )
		if ( ! array_increase_capacity(array) )
			return -1;

	array->elements[array->count++] = element;
	return 0; // Success
}

int array_find_first(const array_t* array, const void* element, size_t start_pos)
{
	for ( size_t index = start_pos; index < array->count; ++index )
		if ( array->elements[index] == element )
			return (int)index;

	return array_not_found;
}

int array_remove_first(array_t* array, const void* element, size_t start_pos)
{
	int index = array_find_first(array, element, start_pos);
	if ( index == array_not_found )
		return -1;

	for ( --array->count; (size_t)index < array->count; ++index )
		array->elements[index] = array->elements[index + 1];
	if ( array->count == array->capacity / 10 )
		array_decrease_capacity(array);
	return 0; // Removed
}
