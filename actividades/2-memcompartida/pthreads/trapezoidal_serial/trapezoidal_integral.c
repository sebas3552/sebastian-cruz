#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "concurrency.h"

double f(double x)
{
	return x*x - 3.0*x + 6.0; //f(x) = x²-3x+6
}

double integral(double a, double b, double n, double (*f) (double))
{
	double delta_x = (b-a)/n;  //average size of each trapezoid, where n is the number of partitions of the interval
	double area = 0;
	for(int i = 0; i <= n; ++i){
		area += i == 0? f(a + (double)i * delta_x) : 2.0 * f(a + (double)i*delta_x);
	}
	area *= delta_x/2.0;
	return area;
}

int main(int argc, char* argv[])
{
	assert(argv);
	if(argc != 4){
		fprintf(stderr, "Missing or extra arguments!\n");
		return -1;
	}
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double n = atof(argv[3]);
	if(!n){
		fprintf(stderr, "Wrong input!\n");
		return -1;
	}
	walltime_t timer;
	walltime_start(&timer);
	double area = integral(a, b, n, f);
	double time_elapsed = walltime_elapsed(&timer);
	printf("The integral from %.2lf to %.2lf of x²-3x+6, with resolution of %d intervals is = %lf\nElapsed: %lfs\n", a, b, (int)n, area, time_elapsed);
	return 0;
}
