#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#include "concurrency.h"

typedef struct thread_data{
	size_t thread_number;
	size_t total_threads;
	size_t* current_thread;
	pthread_t thread;
	pthread_mutex_t* mutex;
} thread_data_t;

void* hello(void* data)
{
	thread_data_t* my_data =  (thread_data_t*) data;
	while(my_data->thread_number != *(my_data->current_thread))
		; //busy-waiting
	printf("Hello from thread %zu of %zu\n", my_data->thread_number, my_data->total_threads);

	pthread_mutex_lock(my_data->mutex);
	++(*my_data->current_thread);
	pthread_mutex_unlock(my_data->mutex);
	return 0;
	
}

int main(int argc, char* argv[])
{
	(void) argc;
	pthread_mutex_t global_mutex;
	pthread_mutex_init(&global_mutex, NULL);
	size_t thread_count = 1;
	size_t current_thread = 1;
	if(argv[1])
		thread_count = (size_t) atoi(argv[1]);
	else
		thread_count = concurrency_cpu_count();
	walltime_t time;
	walltime_start(&time);
	thread_data_t* thread_structs = (thread_data_t*)calloc(thread_count, sizeof(thread_data_t));
	for(size_t index = 0; index < thread_count; ++index){
		thread_structs[index].thread_number = (size_t)index+1;
		thread_structs[index].total_threads = thread_count;
		thread_structs[index].current_thread = &current_thread;
		thread_structs[index].mutex = &global_mutex;
		pthread_create((&thread_structs[index].thread), NULL, hello, (void*) (&thread_structs[index]));
	}
	printf("Hello from main thread\n");
	for(size_t index = 0; index < thread_count; ++index){
		pthread_join(thread_structs[index].thread, NULL); //wait for thread until it finishes it's execution.
	}
	double seconds_elapsed = walltime_elapsed(&time);
	pthread_mutex_destroy(&global_mutex);
	free(thread_structs);
	printf("Elapsed %lfs with %d threads\n", seconds_elapsed, (int)thread_count);
	return 0;
}
