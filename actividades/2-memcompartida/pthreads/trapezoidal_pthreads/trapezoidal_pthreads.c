#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "concurrency.h"

typedef struct data
{
	double a;
	double delta_x;
	double my_n;
	double start;
	double* result;
	double (*f) (double);
	pthread_t thread;
	sem_t* semaphore;
} worker_data_t;

double f(double x)
{
	return x*x - 3.0*x + 6.0; //f(x) = x²-3x+6
}

void* workers_integral(void * data)
{
	worker_data_t* my_data = (worker_data_t*) data;
	double area = 0;
	for(int i = my_data->start; i < my_data->my_n; ++i){
		area += i == 0? f(my_data->a + (double)i * my_data->delta_x) : 2.0 * f(my_data->a + (double)i*my_data->delta_x);
	}
	area *= my_data->delta_x/2.0;
	sem_wait(my_data->semaphore);
	*(my_data->result) += area;
	sem_post(my_data->semaphore);
	return data;
}

/*Function called from main thread that manages the worker threads*/
double main_integral(double a, double b, double n, int total_workers, double (*f) (double))
{
	const int SEM_GREEN = 1;
	double delta_x = (b-a)/(double)n;
	double workers_interval = n/(double)total_workers; //Partition given to each worker
	double start = 0.0;
	double result = 0.0;
	sem_t results_semaphore;
	sem_init(&results_semaphore, 0, SEM_GREEN);
	worker_data_t* workers = (worker_data_t*) calloc(total_workers, sizeof(worker_data_t));
	assert(workers);
	//Gives each worker the data he needs to accomplish his work, and a semaphore to restrict the accumulator variable result
	for(int current = 0; current < total_workers; ++current){
		workers[current].a = a;
		workers[current].delta_x = delta_x;
		workers[current].start = start;
		workers[current].my_n = (current+1)*workers_interval;
		workers[current].result = &result;
		workers[current].semaphore = &results_semaphore;
		workers[current].f = f;
		pthread_create(&(workers[current].thread), NULL, workers_integral, (void*) (&workers[current]));
		start += workers_interval;
	}
	for(int i = 0; i < total_workers; ++i){
		pthread_join(workers[i].thread, NULL);
	}
	free(workers);
	sem_destroy(&results_semaphore);
	return result;
}

int main(int argc, char* argv[])
{
	assert(argv);
	if(argc < 4 || argc > 5){
		fprintf(stderr, "Missing or extra arguments!\n");
		return -1;
	}
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double n = atof(argv[3]);
	if(!n){
		fprintf(stderr, "Wrong input!\n");
		return -1;
	}
	int workers = concurrency_cpu_count();
	if(argc == 5)
		workers = atoi(argv[4]) >= 1? atoi(argv[4]) : workers;
	walltime_t timer;
	walltime_start(&timer);
	double area = main_integral(a, b, n, workers, f);
	double time_elapsed = walltime_elapsed(&timer);
	printf("The integral from %.2lf to %.2lf of x²-3x+6, with resolution of %d intervals is = %lf\nElapsed: %lfs with %d workers\n", a, b, (int)n, area, time_elapsed, workers);
	return 0;
}
