#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#define TRUE 1

static size_t number_count = 0;
static size_t current_step = 0;
static const size_t max_steps = 10;
static size_t* numbers = NULL;
static pthread_barrier_t barrier;
static sem_t* global_semaphores;

int start(int, int, int, int);
int end(int, int, int, int);

typedef struct data
{
	size_t rank;
	size_t start;
	size_t end;
	size_t worker_count;
} my_data_t;

int converged()
{
	for(size_t i = 0; i < number_count; ++i)
		if(numbers[i] != 1)
			return !TRUE;
	return TRUE;
}

void* calculate(void* data)
{
	my_data_t* my_data = (my_data_t*) data;
	const size_t my_id = my_data->rank;
	size_t next = 0;
	size_t prev = 0;

	while (TRUE)
	{
		//critical section: only one thread at a time can read and write in numbers array
		sem_wait(&global_semaphores[my_id]);
		//if current worker has something to do
		if(my_data->start < my_data->end){
			//loop to calculate collatz numbers within its interval
			for(size_t current = my_data->start; current < my_data->end; ++current){
				next = (current + 1) % number_count;
				prev = (int)current-1 < 0 ? number_count-1 : current-1;
				assert(current < number_count);
				if(numbers[current] != 1){
					if ( numbers[current] % 2 == 0 ){			
						numbers[current] /= 2;
					}
					else{
						numbers[current] = numbers[prev] * numbers[current] + numbers[next];		
					}
				}
			}
		}
		//after calculating numbers in its interval, allow other threads to work on the array
		sem_post(&global_semaphores[(my_id+1) % my_data->worker_count]);
		pthread_barrier_wait(&barrier);
		//ending condition
		if(converged() || current_step >= max_steps){		
			pthread_barrier_wait(&barrier);
			return NULL;
		}else{		
			pthread_barrier_wait(&barrier);
			if ( my_id == 0 )
				++current_step;
		}
	}
}

int main()
{
	//get number_count
	printf("Enter the quantity of numbers:\n");
	int valid = scanf("%zu", &number_count);
	if(!valid){
		fprintf(stderr, "Illegal input!\n");
		exit(1);
	}
	//ask for each number
	numbers = malloc( number_count * sizeof(size_t) );
	printf("Enter %zu numbers:\n", number_count);
	for ( size_t index = 0; index < number_count; ++index )
		valid = scanf("%zu", &numbers[index]);
	//validate numbers
	if(!valid){
		fprintf(stderr, "Illegal input!\n");
		exit(1);
	}
	//ask for worker_count
	printf("Enter the number of workers: \n");
	size_t workers_requested = 0;
	scanf("%lu", &workers_requested);
	//more workers than numbers is not allowed, doesn't make sense
	size_t worker_count = workers_requested <= number_count ? workers_requested : number_count;
	puts("Calculation has started!\n");
	pthread_barrier_init(&barrier, NULL, (unsigned)worker_count);
	sem_t semaphores[worker_count];
	global_semaphores = &semaphores[0];
	for(size_t index = 0; index < worker_count; ++index){
		if(index > 0)
			sem_init(&global_semaphores[index], 0, 0);
		else
			sem_init(&global_semaphores[index], 0, 1);
		
	}
	my_data_t* workers_data = (my_data_t*)calloc(worker_count, sizeof(my_data_t));
	int c = number_count / worker_count;
	int r = number_count % worker_count;
	//calculate the start and end point for each worker
	for(size_t index = 0; index < worker_count; ++index){
		workers_data[index].rank = index;
		workers_data[index].start = start(0, index, c, r);
		workers_data[index].end = end(0, index, c, r);
		workers_data[index].worker_count = worker_count;
	}
	
	pthread_t* workers = malloc(worker_count * sizeof(pthread_t));
	for ( size_t index = 0; index < worker_count; ++index )
		pthread_create(&workers[index], NULL, calculate, (void*)&workers_data[index]);
	
	for ( size_t index = 0; index < worker_count; ++index )
		pthread_join(workers[index], NULL);
	

	pthread_barrier_destroy(&barrier);
	for(size_t index = 0; index < worker_count; ++index)
		sem_destroy(&global_semaphores[index]);
		
	free(workers_data);
	free(workers);
	free(numbers);
	if ( current_step < max_steps )
		printf("Converged in %zu steps\n", current_step);
	else
		printf("No converge in %zu steps\n", max_steps);
	return 0;
}

int start(int a, int rank, int c, int r)
{
	return a + rank*c + (rank < r ? rank : r);
}

int end(int a, int rank, int c, int r)
{
	return a + (rank + 1)*c + ((rank + 1) < r ? (rank + 1) : r);
}
