#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <omp.h>

void hello(void)
{
	int thread_number = omp_get_thread_num();
	int thread_count = omp_get_num_threads();
	printf("Hello from thread %d of %d\n", thread_number, thread_count);
}

int main(int argc, char* argv[])
{
	if(argc == 2)
		omp_set_num_threads(atoi(argv[1]));
	#pragma omp parallel
	hello();
	printf("Hello from main thread\n");
	return 0;
}
