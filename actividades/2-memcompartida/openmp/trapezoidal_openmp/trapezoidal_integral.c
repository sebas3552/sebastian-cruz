#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

double f(double x)
{
	return x*x - 3.0*x + 6.0; //f(x) = x²-3x+6
}

void integral(double a, double b, double n, double (*f) (double), int workers, double* global_result)
{
	int thread_number = omp_get_thread_num();
	double interval = b-a;
	double my_area = interval/(double)workers;
	double my_a = a + (thread_number)*my_area;
	double my_b = (a + (thread_number+1)*my_area < b? a + (thread_number+1)*my_area : b);
	double my_n = n / (double) workers;
	double delta_x = (my_b-my_a)/my_n;  //average size of each trapezoid, where n is the number of partitions of the interval
	double area = 0;
	for(int i = 0; i <= my_n; ++i){
		area += i == 0? f(my_a + (double)i * delta_x) : 2.0 * f(my_a + (double)i*delta_x);
	}
	area *= delta_x/2.0;
	#pragma omp critical
	*global_result += area;
}

int main(int argc, char* argv[])
{
	assert(argv);
	if(argc > 5){
		fprintf(stderr, "Missing or extra arguments!\n");
		return -1;
	}
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double n = atof(argv[3]);
	int max_threads = argc == 5? atoi(argv[4]) : omp_get_max_threads();
	omp_set_num_threads(max_threads);
	double result = 0.0;
	double start = omp_get_wtime();
	#pragma omp parallel
	integral(a, b, n, f, max_threads, &result);
	double elapsed = omp_get_wtime() - start;
	printf("The integral from %.2lf to %.2lf of x²-3x+6, with resolution of %d intervals is = %lf\nElapsed: %lfs with %d workers\n", a, b, (int)n, result, elapsed, max_threads);
	return 0;
}
