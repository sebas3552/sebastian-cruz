#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define RANDOMIZE srand(time(0))

void swap(double *x, double* y)
{
	double temp = *x;
	*x = *y;
	*y = temp;
}

double rand_flot()
{
	RANDOMIZE;
	double rand1 = (double) rand();
	RANDOMIZE;
	double rand2 = (double) rand();
	while(rand2 == rand1){
		RANDOMIZE;
		rand2 = rand();
	}
	return rand1/rand2;
}

void serial_odd_even_sort(size_t n, double* arr)
{
	for ( size_t phase = 0; phase < n; ++phase ){
		if ( phase % 2 == 0 ){
			for ( size_t i = 1; i < n; i += 2 )
				if ( arr[i - 1] > arr[i] )
					swap( &arr[i - 1], &arr[i] );
		}
		else{
			for ( size_t i = 1; i < n - 1; i += 2 )
				if ( arr[i] > arr[i + 1] )
					swap( &arr[i], &arr[i + 1]);
		}
	}
}

int main(int argc, char* argv[])
{
	if(argc != 2){
		fprintf(stderr, "Missing or extra arguments!\n");
		exit(1);
	}
	size_t n = (size_t) atoi(argv[1]);
	double* arr = (double*) calloc(n, sizeof(double));
	RANDOMIZE;
	for(size_t i = 0; i < n; ++i){	
		//arr[i] = rand_flot(); //slow way
		arr[i] = (double) (rand() % n); //fast way
	}
	
	serial_odd_even_sort(n, arr);
	/*for(size_t i = 0; i < n; ++i)
		printf("%.2lf ", arr[i]);
	printf("\n");*/
	free(arr);
	return 0;
}
