#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define RANDOMIZE srand(time(0))

static size_t thread_count = 2;

void swap(double *x, double* y)
{
	double temp = *x;
	*x = *y;
	*y = temp;
}

void serial_odd_even_sort(size_t n, double* arr)
{
	for ( size_t phase = 0; phase < n; ++phase ){
		if ( phase % 2 == 0 ){
			#pragma omp parallel for num_threads(thread_count) \
			default(none) shared(arr, n)
			for ( size_t i = 1; i < n; i += 2 )
				if ( arr[i - 1] > arr[i] )
					swap( &arr[i - 1], &arr[i] );
		}
		else{
			#pragma omp parallel for num_threads(thread_count) \
			default(none) shared(arr, n)
			for ( size_t i = 1; i < n - 1; i += 2 )
				if ( arr[i] > arr[i + 1] )
					swap( &arr[i], &arr[i + 1]);
		}
	}
}

int main(int argc, char* argv[])
{
	if(argc != 3){
		fprintf(stderr, "Missing or extra arguments!\n");
		exit(1);
	}
	size_t n = (size_t) atoi(argv[1]);
	thread_count = (size_t) atoi(argv[2]);
	double* arr = (double*) calloc(n, sizeof(double));
	RANDOMIZE;
	for(size_t i = 0; i < n; ++i)
		arr[i] = (double) (rand()%n);
	
	serial_odd_even_sort(n, arr);
	for(size_t i = 0; i < n; ++i)
		printf("%.2lf ", arr[i]);
	printf("\n");
	free(arr);
	return 0;
}
